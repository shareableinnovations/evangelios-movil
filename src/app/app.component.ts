import { AndroidFullScreen } from '@ionic-native/android-full-screen';
import { HomeDonationPage } from './../pages/homeDonation/homeDonation.component';
import { HomePage } from './../pages/home/home';
import { CreditsPage } from './../pages/credits/credits.component';
import { DonationPage } from './../pages/donation/donation.component';
import { MediaComponent } from './../pages/media/media.component';
import {Component, OnInit, ViewChild} from '@angular/core';
import { Nav, Platform, Events, ToastController, LoadingController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoginPage } from "../pages/login/login.component";
import { PlayListPage } from '../pages/playLists/playList.component';
import { DataService } from '../shared/services/dataService';
import { FavoriteListPage } from '../pages/favoriteLists/favoriteLists.component';
import { CategoryListPage } from '../pages/categoryList/categoryList.component';
import { SearchListPage } from '../pages/search/search.component';
import { ConfigPage } from '../pages/config/config.component';
import { SongsPage } from "../pages/songList/songList.component";
import { SocialSharing } from '@ionic-native/social-sharing';
import { NativePageTransitions, NativeTransitionOptions } from "@ionic-native/native-page-transitions";
import {CommentsPage} from '../pages/comments/comments.component';
import { UtilService } from "../shared/services/utilService";
import { AndroidPermissions } from "@ionic-native/android-permissions";
import { User } from "../shared/model/user";
import { Camera, CameraOptions } from "@ionic-native/camera";
import { UserService } from "../shared/services/userService";
import { BaseComponent } from "../shared/components/base.component";
import { ImagePicker } from "@ionic-native/image-picker";

@Component({
  templateUrl: 'app.html'
})
export class MyApp extends BaseComponent implements OnInit {
  @ViewChild(Nav) nav: Nav;

  //rootPage: any = HomePage;
  user: User;
  avatar: any;
  rootPage: any;
  options: any;
  pages: Array<{ title: string, component: any, icon: string }>;
  navClass: string;
  timer: any;
  constructor(public platform: Platform,
              public statusBar: StatusBar,
              public splashScreen: SplashScreen,
              public dataService: DataService,
              private socialSharing: SocialSharing,
              private androidFullScrenn: AndroidFullScreen,
              private events: Events,
              private nativePageTransitions: NativePageTransitions,
              private utilService: UtilService,
              private androidPermissions: AndroidPermissions,
              private camera: Camera,
              private userService: UserService,
              protected toastCtrl: ToastController,
              protected loadingController: LoadingController) {
    super(toastCtrl, loadingController);
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Inicio', component: HomeDonationPage, icon: "home" },
      { title: 'Buscar', component: SearchListPage, icon: "search" },
      { title: 'Listas de Reproducción', component: PlayListPage, icon: "musical-note" },
      // { title: 'Categorias', component: CategoryListPage, icon: "folder" },
      { title: 'Favoritos', component: FavoriteListPage, icon: "heart" },
      // { title: 'Donaciones', component: DonationPage, icon: "thumbs-up" },
      { title: 'Mi Perfil', component: ConfigPage, icon: "settings" },
      { title: 'Creditos', component: CreditsPage, icon: "happy" },
      { title: 'Sugerencias', component: CommentsPage, icon: "paper" }

    ];

    const self = this;
    this.platform.ready().then(() => {
      if (this.platform.is('ios')) {

      } else if (this.platform.is('android')) {
        console.log('entre')
        self.options = { maximumImagesCount: 1 };
        self.checkPermissions()
      }
    });
  }

  ngOnInit() {
    this.timer = Math.floor(Math.random()*(8400000-6400000+1)+6400000);
    setInterval(() => {
      // 86400000
      if (this.nav.getActive().name !== 'DonationPage') {
        this.utilService.goToPage(DonationPage, 'push', 'down');
      }

    }, this.timer)
  }

  initializeApp() {
    this.navClass = 'full';
    this.platform.ready().then(() => {
      if (this.platform.is('android')) {
        this.androidFullScrenn.isImmersiveModeSupported().then(() => this.androidFullScrenn.immersiveMode())
          .catch(err => console.log(err));
      }
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.backgroundColorByName('white');
      this.statusBar.styleLightContent();
      this.splashScreen.hide();
      // this.dataService.setAvatar("../assets/img/avatars/noavatar.png")
      this.dataService.setMiniPlayer(false)
      this.dataService.setPseudoPlayer(false);
      if (this.dataService.getRemember()) {
        if (localStorage.getItem('userRemembered') !== "") {
          this.dataService.setAutoLog(true);
          this.navClass = 'withPlayer'
          this.rootPage = HomeDonationPage;
          //this.rootPage = MediaComponent;
        } else {
          this.dataService.setAutoLog(false);
          this.rootPage = HomePage;
        }
      }
      else {
        this.dataService.setAutoLog(false);
        this.rootPage = HomePage;
      }

      this.events.subscribe('goToMediaPlayer', () => {
        let options: NativeTransitionOptions = {
        direction: 'up',
        duration: 500,
        slowdownfactor: -1,
        iosdelay: 100,
        androiddelay: 150
      };
      this.nativePageTransitions.slide(options);
      this.nav.push(MediaComponent,{},{animate: false});
      })
    })
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    // if (page.title === 'Donaciones') {
    //   window.open('https://saxum.org/friends-of-saxum/donate/', '_system')
    // } else {
    /* this.nav.setRoot(page.component); */
    this.utilService.goToPage(page.component, 'root');
    //}
  }

  share(option) {
    // console.log('entre')
    if (option === 1) {
      // console.log('entre')
      this.socialSharing.shareViaTwitter("Evangelios Narrados").then().catch()
    }
    else if (option === 2) {
      // console.log('entre')
      this.socialSharing.shareViaFacebook("Evangelios Narrados").then().catch()
    }
    else if (option === 3) {
      this.socialSharing.shareViaWhatsApp("Evangelios Narrados").then().catch()
    }
    else if (option === 4) {
      this.socialSharing.shareViaInstagram("Evangelios Narrados", null).then().catch()
    }
  }

  pickImage() {
    this.takePhoto();
  }

  takePhoto() {
    const self = this;
    const options: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      sourceType: 0,
    };

    self.camera.getPicture(options).then((imageData) => {
      this.user = this.dataService.getUserLoged();
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      this.user.image = base64Image;
      console.log(this.user);
      self.presentLoadingDefault();
      self.userService.updateUser(this.user).then(async (result) => {
        if (result.error) {
          self.dismissLoader();
        } else {
          await self.dataService.setAvatar(base64Image);
          self.dismissLoader();
        }
      })
    }, (err) => {
      // Handle error
    });
  }

  checkPermissions() {
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE)
      .then(result => {
        if (result.hasPermission === false) {
          this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE])
        }
      })
  }

  async avatarGet() {
    const self = this;
    return await self.dataService.getAvatar();
  }

}
