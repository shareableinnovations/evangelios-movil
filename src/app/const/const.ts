export const Path = {
    USER: '/users/',
    FAVORITES: '/users/favorites',
    AUTHENTICATION: '/users/authenticate',
    ALBUM: '/albums',
    CATEGORY: '/categories',
    SONG: '/songs',
    COMMENT: '/comments'
};


export const Environment = {
    production: true,
    modoDebug: false,
    apiUrl:'https://spoti-api.devups.io/rest-api',
    // apiUrl:'http://192.168.254.63:9000/rest-api',
    formatoFecha: 'yyyy-MM-dd'
};

