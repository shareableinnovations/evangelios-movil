import { CommentService } from './../shared/services/commentService';
import { ComponentsModule } from './../shared/components/components.module';
import { ForgotPage } from './../pages/forgotPassword/forgotPassword.component';
import { CreditsPage } from './../pages/credits/credits.component';
import { HomeDonationPage } from './../pages/homeDonation/homeDonation.component';
import { Network } from '@ionic-native/network';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { Http, HttpModule } from "@angular/http";
import {StatusBar} from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoginPage } from "../pages/login/login.component";
import { PlayListPage } from '../pages/playLists/playList.component';
import { DataService } from '../shared/services/dataService';
import { FavoriteListPage } from '../pages/favoriteLists/favoriteLists.component';
import { CategoryListPage } from '../pages/categoryList/categoryList.component';
import { SearchListPage } from '../pages/search/search.component';
import { ConfigPage } from '../pages/config/config.component';
import { SongsPage } from '../pages/songList/songList.component';
import { CommentsPage } from '../pages/comments/comments.component';
import { Media, MediaObject } from "@ionic-native/media";
import { FileTransfer, FileTransferObject, FileUploadOptions } from "@ionic-native/file-transfer";
import { File } from '@ionic-native/file';
import { UserService } from "../shared/services/userService";
import { RegisterPage } from "../pages/register/register.component";
import { HttpService } from "../shared/services/httpService";
import { AlbumService } from "../shared/services/albumService";
import { CategoryService } from "../shared/services/categoryService";
import { SongService } from "../shared/services/songService";
import { SocialSharing } from '@ionic-native/social-sharing';
import { MusicControls } from '@ionic-native/music-controls';
import { MediaComponent } from "../pages/media/media.component";
import { DonationPage } from "../pages/donation/donation.component";
import { ImagePicker } from '@ionic-native/image-picker';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ImageStoreProvider } from '../providers/image-store/image-store';
import { IonicStorageModule } from '@ionic/storage';
import { AndroidFullScreen } from '@ionic-native/android-full-screen';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { UtilService } from "../shared/services/utilService";
import { InfoPage } from "../pages/info/info";
import { PaypalPage } from "../pages/paypal/paypal.component";
import { PreviewPage } from "../pages/preview/preview";
import { InAppBrowser } from '@ionic-native/in-app-browser';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    PlayListPage,
    FavoriteListPage,
    CategoryListPage,
    SearchListPage,
    ConfigPage,
    SongsPage,
    RegisterPage,
    MediaComponent,
    DonationPage,
    PaypalPage,
    HomeDonationPage,
    CreditsPage,
    ForgotPage,
    CommentsPage,
    InfoPage,
    PreviewPage,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp,  {backButtonText: '',}),
    IonicStorageModule.forRoot(),
    ComponentsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    PlayListPage,
    FavoriteListPage,
    CategoryListPage,
    SearchListPage,
    ConfigPage,
    SongsPage,
    RegisterPage,
    MediaComponent,
    DonationPage,
    PaypalPage,
    HomeDonationPage,
    CreditsPage,
    ForgotPage,
    CommentsPage,
    InfoPage,
    PreviewPage,
  ],
  providers: [
    File,
    Media,
    FileTransfer,
    FileTransferObject,
    StatusBar,
    SplashScreen,
    DataService,
    UserService,
    HttpService,
    AlbumService,
    CategoryService,
    SongService,
    SocialSharing,
    MusicControls,
    Network,
    ImagePicker,
    AndroidPermissions,
    Camera,
    ImageStoreProvider,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ImageStoreProvider,
    AndroidFullScreen,
    NativePageTransitions,
    UtilService,
    CommentService,
    InAppBrowser
  ]
})
export class AppModule {}
