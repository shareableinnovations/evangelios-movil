import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

const STORAGE_KEY = 'imageStorage';

@Injectable()
export class ImageStoreProvider {

  constructor(public storage: Storage) {
    console.log('Hello ImageStoreProvider Provider');
  }

  addImage(image) {
    let found = false;
    return this.getAllImages().then(result => {
      if (result) {
        result.forEach(element => {
          if (element.imageName === image.imageName) {
            found = true;
          }
        });
        if (!found) {
          result.push(image);
          return this.storage.set(STORAGE_KEY, result);
        }
      } else {
        return this.storage.set(STORAGE_KEY, [image]);
      }
    });
  }


  getAllImages() {
    return this.storage.get(STORAGE_KEY);
  }

}
