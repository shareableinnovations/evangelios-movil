import { PseudoPlayerComponent } from './pseudoPlayer/pseudoPlayer.component';
import { MiniPlayerComponent } from './miniPlayer/miniPlayer.component';
import { CardSongComponent } from './card-song/card-song.component';
import { AlbumComponent } from './album/album.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from 'ionic-angular';

@NgModule({
  imports: [
    IonicModule,
    CommonModule
  ],
  declarations: [
    AlbumComponent,
    CardSongComponent,
    MiniPlayerComponent,
    PseudoPlayerComponent
  ],
  exports: [
    AlbumComponent,
    CardSongComponent,
    MiniPlayerComponent,
    PseudoPlayerComponent
  ]
})
export class ComponentsModule { }