import { ToastController, LoadingController, Loading } from "ionic-angular";

export class BaseComponent {

  loader: Loading;

  constructor(protected toastCtrl: ToastController, protected loadingCtrl: LoadingController) {

  }


  presentToastCenter(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 1000,
      position: 'middle'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  presentLoadingDefault(message?) {
    this.loader = this.loadingCtrl.create({
      content: message && message || ''
    });

    this.loader.present();
  }

  dismissLoader(){
    this.loader.dismiss();
  }
}
