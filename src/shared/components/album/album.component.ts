import { ImageStoreProvider } from './../../../providers/image-store/image-store';
import { DataService } from './../../services/dataService';
import { BaseComponent } from "../base.component";
import { Component, EventEmitter, Input, Output } from "@angular/core";
import { ToastController, LoadingController, Platform, Loading, normalizeURL } from "ionic-angular";
import { File } from '@ionic-native/file';
import { FileTransferObject, FileTransfer } from "@ionic-native/file-transfer";
import { Media, MediaObject } from "@ionic-native/media";
import {UtilService} from "../../services/utilService";
import {DonationPage} from "../../../pages/donation/donation.component";
import {InfoPage} from "../../../pages/info/info";
import {SongService} from "../../services/songService";
import { Network } from '@ionic-native/network';
import {HomeDonationPage} from "../../../pages/homeDonation/homeDonation.component";


@Component({
  selector: 'album-card',
  templateUrl: 'album.component.html',
})

export class AlbumComponent extends BaseComponent {
  @Input('showToggle') showToggle: boolean;
  @Input('album') album: any;
  @Input('isAlbum') isAlbum: boolean;
  @Input('category') category: boolean = false;
  @Output() showAlbum = new EventEmitter();
  fileImage: MediaObject;
  checked: boolean;
  image: any;
  detail: any;
  number: number;
  base64Image: any;
  storedImages: Array<any>;
  storageDirectory: any;
  songs: Array<any> = [];
  download: boolean;
  imageDownloaded: boolean = false;
  counter = 0;
  showIcon = false;
  isDownloading = false;
  allfound = true;
  onConnect: any;
  onDisconnect: any;
  loader: Loading;

  constructor(public imageProvider: ImageStoreProvider, protected toastCtrl: ToastController,
    protected loadingController: LoadingController, private dataService: DataService,
    private platform: Platform, private file: File, private transfer: FileTransfer, private media: Media,
              private utilService: UtilService,
              private songService: SongService,
              private network: Network) {
    super(toastCtrl, loadingController)
    this.image = "assets/img/avatars/noavatar.png";
    // assign storage directory
    this.platform.ready().then(() => {
      if (this.platform.is('ios')) {
        this.storageDirectory = this.file.dataDirectory;
      } else if (this.platform.is('android')) {
        this.storageDirectory = this.file.externalDataDirectory;
      }
    });
  }
 /* ionViewDidEnter() {
    this.checkAlbumNetWork();
  }*/
  ionViewWillLeave(){
    this.onConnect.unsubscribe();
    this.onDisconnect.unsubscribe();
  }

  async ngOnInit() {
    console.log(this.album);
    // this.downloadImages();
    // await this.checkDownloadedAlbum()
    if (this.showToggle && this.network.type !== 'unknown' && this.network.type !== 'none') {
      if (localStorage.getItem('downloads')) {
        let albums_downloaded: Array<any> = JSON.parse(localStorage.getItem('downloads'));
        let index = albums_downloaded.findIndex(element => element.album === this.album.name);
        if (index !== -1) {
          if (albums_downloaded[index].selected) {
            this.checked = true;
            await this.prepareSongstoDownload();
            this.counter = 0;
            await this.downloadAlbum();
            this.isDownloading = false;
          }
          else {
            this.checked = false;
          }
        }
      }else {
        console.log('no descargar');
      }
    }else {
      if (localStorage.getItem('downloads')) {
        let albums_downloaded: Array<any> = JSON.parse(localStorage.getItem('downloads'));
        let index = albums_downloaded.findIndex(element => element.album === this.album.name);
        if (index !== -1) {
          if (albums_downloaded[index].selected) {
            this.checked = true;
          }
          else {
            this.checked = false;
          }
        }
      }
    }

    /*if (this.imageDownloaded) {
      if (this.platform.is('ios')) {  //ios
        this.image = (this.storageDirectory).replace(/^file:\/\//, '') + '/' + this.album.name + ".png";
      } else {  // android
        this.image = this.storageDirectory + this.album.name + ".png";
      }
      this.album.image = this.image;
    }
    else {
      if (this.album.image) {
        this.image = this.album.image;
      }
    }*/
    this.number = 0 ;
    setTimeout(() => {
      this.preparePistas();
    },2000);
  }

  async preparePistas() {
    let albums: any = JSON.parse(localStorage.getItem('albums'));
    let songsCount: any = JSON.parse(localStorage.getItem('songsCount'));

    if (this.network.type !== 'unknown' && this.network.type !== 'none') {
      if (this.dataService.songsCount && this.dataService.songsCount.length > 0) {
        this.dataService.songsCount.forEach(element => {
          if (element.album_id === this.album.id) {
            this.number = element.count;
          }
        });
      }
    } else {
      if (albums !== null && albums.length > 0 && songsCount !== null && songsCount.length > 0) {
        songsCount.forEach(element => {
          if (element.album_id === this.album.id) {
            this.number = element.count;
          }
        });
      }
    }
    console.log(this.number);
  }

  async prepareSongs() {
    this.number = 0;
    if (this.isAlbum) {
      this.dataService.getSongs().forEach(element => {

        const inCategory: boolean = element.albums.find(element => {
          return element._id === this.album.id;
        })

        if (inCategory) {
          this.number++
        }
      });
    } else {
      this.dataService.getSongs().forEach(element => {

        const inCategory: boolean = element.categories.find(element => {
          return element._id === this.album.id;
        })

        if (inCategory) {
          this.number++
        }
      });
    }
  }

  openInfo() {
    this.utilService.goToPageParams(InfoPage, 'root',this.album);
  }

  async downloadAlbum() {
    try {
      await this.platform.ready();
      for (const element of this.songs) {
        await this.downloadSong(element);
      }
      this.showIcon = true;
    } catch (err) {
      this.presentToast(err.message);
    }
  }

  async downloadSong(element) {
    try {
      const data = await this.checkFile(element.name + ".mp3");
      if (!data) {
        console.log(element);
        const fileTransfer: FileTransferObject = this.transfer.create();
        const entry = await fileTransfer.download(element.audio, this.storageDirectory + element.name + ".mp3.temp");
        await this.moveFile(element.name + ".mp3.temp", element.name + ".mp3");
        console.log('download complete' + entry.toURL());
      }
      return true;
    } catch (err) {
      console.log(err);
      return false;
    }
  }


  async downloadImages() {
    try {
      const data = await this.checkFile(this.album.name + ".png");
      if (!data) {
        const fileTransfer: FileTransferObject = this.transfer.create();
        const entry = await fileTransfer.download(this.album.image, this.storageDirectory + this.album.name + ".png");
        if (entry) {
          console.log('download image')
          const resolvedDirectory = await this.file.resolveDirectoryUrl(this.storageDirectory);
          this.image = resolvedDirectory.nativeURL.replace(/^file:\/\//, '')+this.album.name+ ".png";
        }
        return true;
      }else {
        await this.platform.ready();
        if (this.platform.is('ios')) {
          this.storageDirectory = this.file.dataDirectory;
        }
        if (this.platform.is('android')) {
          this.storageDirectory = this.file.externalDataDirectory;
        }
        console.log('no download image')

        const resolvedDirectory = await this.file.resolveDirectoryUrl(this.storageDirectory);
        this.image =  resolvedDirectory.nativeURL.replace(/^file:\/\//, '')+this.album.name+ ".png";
      }
    } catch (err) {
      console.log(err);
      return false;
    }
  }

  async checkFile(path) {
    try {
      await this.platform.ready();
      if (this.platform.is('ios')) {
        this.storageDirectory = this.file.dataDirectory;
      }
      if (this.platform.is('android')) {
        this.storageDirectory = this.file.externalDataDirectory;
      }
      const resolvedDirectory = await this.file.resolveDirectoryUrl(this.storageDirectory);
      const data = await this.file.checkFile(resolvedDirectory.nativeURL, path);
      return data;
    } catch (err) {
      if (err.code === 1) {
        return null;
      }
      throw err;
    }
  }

  async moveFile(path, newPath) {
    try {
      await this.platform.ready();
      if (this.platform.is('ios')) {
        this.storageDirectory = this.file.dataDirectory;
      }
      if (this.platform.is('android')) {
        this.storageDirectory = this.file.externalDataDirectory;
      }
      const resolvedDirectory = await this.file.resolveDirectoryUrl(this.storageDirectory);
      await this.file.moveFile(resolvedDirectory.nativeURL, path, resolvedDirectory.nativeURL, newPath);
    } catch (err) {
      throw err;
    }
  }


  prepareSongstoDownload() {

    let songs: Array<any> = JSON.parse(localStorage.getItem('songs'));
    console.log(songs);
    if (songs.length === 0 || this.dataService.getSongs() === undefined) {
      this.dataService.setSongs(JSON.parse(localStorage.getItem('songs')));
    }
    console.log(this.dataService.getSongs());

    this.dataService.getSongs().forEach(element => {
      const inCategory = element.albums.find(child => child._id === this.album.id);
      if (inCategory) {
        this.songs.push(element)
      }
    })
  }

  async downloadMusic(event) {
    //if (!this.isDownloading) {
    if (this.network.type === 'unknown' || this.network.type === 'none') {
      this.toastCtrl.create({
        message: 'No tiene conexión a internet',
        duration: 3000,
        position: 'top'
      }).present();

      if (this.checked) {
        this.checked = false;
      }
    } else {
      this.isDownloading = true;
      setTimeout(async () => {
        let downloads: Array<any> = JSON.parse(localStorage.getItem('downloads'));
        if (downloads == null) {
          downloads = [];
          downloads.push({ 'album': this.album.name, 'selected': this.checked })
        }
        else {
          let index = downloads.findIndex(element => element.album === this.album.name);
          if (index === -1) {
            downloads.push({ 'album': this.album.name, 'selected': this.checked })
          }
          else {
            downloads[index].selected = this.checked;
          }
        }
        localStorage.setItem('downloads', JSON.stringify(downloads));

        await this.prepareSongstoDownload();
        this.counter = 0;
        await this.downloadAlbum();
        this.isDownloading = false;
      }, 1000)
    }
    //}
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'top'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  showAlbumClick() {
    this.showAlbumEvent();
  }

  checkAlbumNetWork() {
    // watch network for a disconnect
    /*this.onDisconnect = this.network.onDisconnect().subscribe((data) => {
      console.log(data);
      console.log('network was disconnected :-(');
      localStorage.setItem('disconnected', 'true');
      this.toastCtrl.create({
        message: 'network was disconnected :-(',
        duration: 3000,
        position: 'top'
      }).present();
    });

    this.onConnect = this.network.onConnect().subscribe((data) => {
      console.log(data);

      console.log('network connected!');
      // We just got a connection but we need to wait briefly
      // before we determine the connection type. Might need to wait.
      // prior to doing any api requests as well.
      if (this.network.type === 'wifi') {
        console.log('we got a wifi connection, woohoo!');
        this.toastCtrl.create({
          message: 'we got a wifi connection, woohoo!',
          duration: 3000,
          position: 'top'
        }).present();
      }

      this.toastCtrl.create({
        message: 'network connected!',
        duration: 3000,
        position: 'top'
      }).present();

      localStorage.removeItem('disconnected');
    });*/

    if (this.network.type === 'wifi') {
      this.toastCtrl.create({
        message: 'wifi network connected!',
        duration: 3000,
        position: 'top'
      }).present();
    }

    if (this.network.type === 'unknown') {
      this.toastCtrl.create({
        message: 'unknown network connected!',
        duration: 3000,
        position: 'top'
      }).present();
    }

    if (this.network.type === 'none') {
      this.toastCtrl.create({
        message: 'none network connected!',
        duration: 3000,
        position: 'top'
      }).present();
    }

    if (this.network.type === 'cellular') {
      this.toastCtrl.create({
        message: 'cellular network connected!',
        duration: 3000,
        position: 'top'
      }).present();
    }
  }

  async showAlbumEvent() {
    this.presentLoadingDefault();
    let songs: Array<any> = JSON.parse(localStorage.getItem('songs'));
    let connect: any = localStorage.getItem('disconnected');
    //this.presentLoadingDefault();
    if (this.network.type === 'unknown' || this.network.type === 'none') {
      if (songs == null) {
        this.utilService.goToPage(InfoPage, 'root');
        this.dataService.setSongs([]);
      } else {
        this.dataService.setSongs(JSON.parse(localStorage.getItem('songs')));
      }
    } else {
      await this.songService.getAllSongsAlbum(this.album.id).then(result => {
        if (result.error) {
          if (songs == null) {
            this.utilService.goToPage(InfoPage, 'root');
          }

          this.dataService.setSongs(JSON.parse(localStorage.getItem('songs')));
          throw new Error(result.error);
        }
        else {
          let songsNew = [];

          if (result && result.data && result.data.length > 0) {
            for (let i = 0; i < result.data.length; i+=1) {
              songsNew.push(result.data[i]);
            }
          }

          if (songs !== null) {
            let songsParser = JSON.parse(localStorage.getItem('songs'));
            if (songsParser !== null) {
              for (let i = 0; i < songsParser.length; i+=1) {
                songsNew.push(songsParser[i]);
              }
            }
          }

          let hash = {};
          let array = songsNew.filter(function(current) {
            let exists = !hash[current.id] || false;

            hash[current.id] = true;
            return exists;
          });

          localStorage.setItem('songs', JSON.stringify(array));

          this.dataService.setSongs(array);
        }
      });
    }
    //this.dismissLoader();

    this.showAlbum.emit(this.album);

    this.dismissLoader();
  }

  async checkDownloadedAlbum() {
    await this.prepareSongstoDownload();
    this.allfound = true;
    if (this.songs.length <=0 ){
      this.allfound = false;
    }
    let promises = await this.songs.map(async element => {
      try {
        console.log(element)
        await this.platform.ready();
        if (this.platform.is('ios')) {
          this.storageDirectory = this.file.dataDirectory;
        }
        if (this.platform.is('android')) {
          this.storageDirectory = this.file.externalDataDirectory;
        }
        const resolvedDirectory = await this.file.resolveDirectoryUrl(this.storageDirectory);
        console.log('antes de data')
        const data = await this.file.checkFile(resolvedDirectory.nativeURL, element.name + ".mp3");
        console.log(data);
      }
      catch (error){
        console.log('despues de data')
        this.allfound = false;
      }
    })
    Promise.all(promises).then( results =>{
      console.log(this.allfound);
    })
  }
}
