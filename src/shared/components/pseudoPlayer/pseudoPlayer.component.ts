import { DataService } from './../../services/dataService';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { MusicControls } from '@ionic-native/music-controls';
import { ToastController, LoadingController, Events, Loading } from 'ionic-angular';
import {ChangeDetectorRef, Component} from '@angular/core';
import { Media, MediaObject } from '@ionic-native/media';
import { Platform } from 'ionic-angular/platform/platform';
import { File } from '@ionic-native/file';
import { FileTransferObject, FileTransfer } from "@ionic-native/file-transfer";
import {Network} from "@ionic-native/network";
import {BaseComponent} from "../base.component";

@Component({
  selector: 'pseudo-player',
  templateUrl: 'pseudoPlayer.component.html',

})

export class PseudoPlayerComponent extends BaseComponent{

  showLoading = false;
  position = 0;
  duration = 0;
  file: MediaObject;
  playingSong: boolean = false;
  get_position_interval: any;
  songs = [];
  pos = 0;
  timerTemp = 0;
  volume = 50;
  songListComponent: any;
  songToPlay: any;
  downloaded: boolean = false;
  storageDirectory: any;
  _currentPosition = 0;
  timerInterval: any;
  connectionNetwork: boolean = false;
  loader: Loading;

  constructor(private dataService: DataService, private media: Media,
    private musicControls: MusicControls,
    private events: Events, private platform: Platform,
              private savedFile: File,
              private transfer: FileTransfer,
              private changeDectect: ChangeDetectorRef,
              protected loadingController: LoadingController,
              private network: Network,
              toastCtrl: ToastController) {
    super(toastCtrl, loadingController)

  }

  presentLoadingDefault(message?: any) {
    this.loader = this.loadingCtrl.create({
      content: message && message || ''
    });

    this.loader.present();
  }

  dismissLoader() {
    if (this.loader) {
      this.loader.dismiss();
    }
  }

  async ngOnInit() {
    if (!this.dataService.getListeningEvents()) {
      await this.prepareEvents();
    }
  }

  ionViewDidLoad() {
    // this.prepareEvents();
  }


  async ngAfterViewInit() {
    this.songs = this.dataService.getSongsToPlay();
    // console.log(this.songs)
  }

  setTimer() {
    const self = this;
    // this.timerInterval = setInterval(function () {
    //   if (self.dataService.getTimer() > 5) {
    //
    //   }
    // }, 1000)
  }

  async prepareEvents() {
    this.dataService.setListeningEvents(true);
    const self = this;
    await this.events.subscribe('playMusic', async () => {
      console.log('playMusic')
      this.dataService.setFirstTime(true);
      this.playingSong = this.dataService.getPlaying();
      this.songToPlay = this.dataService.getSongToPlay();
      await self.preparePlatform();
      if (!this.connectionNetwork) {
        self.prepareAll();
      }
    })
    this.events.subscribe('play', async () => {
      this.presentLoadingDefault();
      self.play()
      this.dismissLoader();
    })
    this.events.subscribe('pause', () => {
      this.presentLoadingDefault();
      self.pause();
      this.dismissLoader();
    })
    this.events.subscribe('next', () => {
      console.log('nexxt')
      this.presentLoadingDefault();
      self.nextSong();
      this.dismissLoader();
    })
    this.events.subscribe('previous', () => {
      this.presentLoadingDefault();
      self.previousSong();
      this.dismissLoader();
    })
  }

  async preparePlatform() {
    await this.platform.ready().then(async () => {
      if (this.platform.is('ios')) {
        this.storageDirectory = this.savedFile.dataDirectory;
      } else if (this.platform.is('android')) {
        console.log('ANDROID')
        this.storageDirectory = this.savedFile.externalDataDirectory;
      }

      console.log('resolve')
      await this.savedFile.resolveDirectoryUrl(this.storageDirectory).then(async (resolvedDirectory) => {
        console.log("resolved  directory: ", resolvedDirectory.nativeURL);
        await this.savedFile.checkFile(resolvedDirectory.nativeURL, this.songToPlay.name + ".mp3").then((data) => {
          if (data == true) {  // exist
            console.log('entre download')
            this.downloaded = true;

          } else {  // not sure if File plugin will return false. go to download
            console.log("not found!");
            this.downloaded = false;
            throw { code: 1, message: "NOT_FOUND_ERR" };

          }
        }).catch(async err => {
          this.downloaded = false;
          console.log("Error occurred while checking local files:");
          console.log(err);
          if (this.network.type === 'none' || this.network.type === 'unknown') {
            this.toastCtrl.create({
              message: 'No tiene conexión a internet',
              duration: 3000,
              position: 'top'
            }).present();
            this.connectionNetwork = true;
            this.dataService.setPseudoPlayer(false);
          }
          if (err.code == 1) {
            // not found! download!
            this.downloaded = false;
          }
        });
      });
    });
  }

  prepareAll() {
    console.log(this.dataService.getPlayingName())
    if (this.dataService.getPlayingName() !== this.songToPlay.name) {

      if (this.dataService.getPlayingSong() === undefined || this.dataService.getPlayingSong() === null) {
        console.log('downloaded -->', this.downloaded)
        if (this.downloaded) {
          console.log('descargada')
          if (this.platform.is('ios')) {  //ios
            this.file = this.media.create((this.storageDirectory).replace(/^file:\/\//, '') + '/' + this.songToPlay.name + ".mp3");
          } else {  // android
            this.file = this.media.create(this.storageDirectory + this.songToPlay.name + ".mp3");
          }
        }
        else {
          console.log('creando audio', this.songToPlay)
          this.file = this.media.create(this.songToPlay.audio);
        }
        this.dataService.setPlayingSong(this.file);
        this.dataService.setPlayingName(this.songToPlay.name)
        this.playingSong = false;
        this.dataService.setDuration(0);
        this.dataService.setPosition(0);
        this.playMusic();

      } else {
        console.log('entre', this.dataService.getPlayingSong())
        this.dataService.setfromAnotherSong(true)
        this.dataService.getPlayingSong().release();
        console.log('downloaded', this.downloaded)
        if (this.downloaded) {
          if (this.platform.is('ios')) {  //ios
            this.file = this.media.create((this.storageDirectory).replace(/^file:\/\//, '') + '/' + this.songToPlay.name + ".mp3");
          } else {  // android
            this.file = this.media.create(this.storageDirectory + this.songToPlay.name + ".mp3");
          }
        }
        else {
          console.log('creando streaming')
          this.file = this.media.create(this.songToPlay.audio);
        }
        this.dataService.setPlayingSong(this.file);
        this.dataService.setPlayingName(this.songToPlay.name)
        this.playingSong = false;
        this.dataService.setDuration(0);
        this.dataService.setPosition(0);
        this.playMusic();
      }

    } else {
      this.file = this.dataService.getPlayingSong();
      this.setInterval();
    }
  }

  playMusic() {
    const self = this;
    this._currentPosition = 0;
    this.dataService.setShowLoading(true);
    this.playingSong = true;
    this.dataService.setPlaying(true);
    // to listen to plugin events:
    this.file.play();
    this.dataService.setTimerReload(0);
    this.setInterval();
  }

  setInterval() {
    let timer = 0;
    this.dataService.setTimerReload(0);
    this.dataService.setTimerTemporizador(0);
    const self = this;
    clearInterval(self.get_position_interval);
    this.file.onStatusUpdate.subscribe(status => console.log(status)); // fires when file status changes

    this.file.onSuccess.subscribe(() => {
      console.log('Action is successful')
    });
    this.file.onError.subscribe(error => console.log('Error!', error));

    // get current playback position
    this.file.getCurrentPosition().then((position) => {
      console.log(position);
    });

    this.duration = this.file.getDuration();
    console.log(this.duration);
    let get_duration_interval = setInterval(function () {
      if (self.duration == -1) {
        self.dataService.setShowLoading(true);
        console.log(3);
        self.duration = ~~(self.file.getDuration());
        self.dataService.setDuration(self.duration);
      } else {
        clearInterval(get_duration_interval);
      }
    }, 1000);

    let diff = 2;
    this.get_position_interval = setInterval(function () {
      let last_position = self.dataService.position;
      console.log(last_position, 'last_position');
      if (last_position === 0) {
        last_position++
      }
      self.file.getCurrentPosition().then((position) => {
        if (position === last_position) {
          if (self.playingSong)
            if(self.duration != 0)
              self.dataService.setShowLoading(true);
        }
        else {
          if (self.duration > 0) {
            if (position > 0)
              self.dataService.setShowLoading(false);
          }
        }

        let setTimerReload = JSON.parse(localStorage.getItem('timer'));
        if(setTimerReload != null && setTimerReload > 0) {
          self.dataService.setTimer(setTimerReload);
        }
        self.dataService.setTimerReload(self.dataService.getTimer() >= 50 ? (self.dataService.getTimer() / 10) - self.dataService.getTimerTemporizador() : 0);

        if (position >= 0 && position < self.dataService.getDuration() && position > last_position) {
          console.log(position);
          if (self.dataService.position != position && self.dataService.getTimer() >= 50) {
            timer++
            console.log(timer, 'timer');
            self.dataService.setTimerTemporizador(timer/10);
            console.log(self.dataService.getTimer(), 'self.dataService.getTimer()');
            if (timer > self.dataService.getTimer()) {
              self.file.stop();
              self.dataService.setPlaying(false)
              self.playingSong = false;
              self.dataService.setTimer(0);
              self.dataService.setTimerReload(0);
              localStorage.setItem('timer', JSON.stringify(0));
              clearInterval(self.get_position_interval);
            }
          }
          if (Math.abs(last_position - position) > diff) {
            // set position
            self.file.seekTo(last_position * 1000);
          } else {
            // update position for display
            self.dataService.position = position;
          }
        } else if (position >= self.dataService.getDuration() && self.dataService.getDuration() > 0) {
          self.nextSong();
          self.events.publish('changeLyrics');
          if(self.dataService.getTimerReload() > 0) {
            localStorage.setItem('timer',JSON.stringify(self.dataService.getTimerReload() * 10))
          }
        }
      });
    }, 100);

    this.prepareMusicControls();

  }

  pauseMusic() {
    this.file.pause();
    this.dataService.setPlaying(false)
    this.playingSong = false;
  }

  prepareMusicControls() {
    let self = this;
    this.musicControls.create({
      track: self.songToPlay.name,
      artist: '',
      isPlaying: true,                         // optional, default : true
      dismissable: true,                         // optional, default : false

      // hide previous/next/close buttons:
      hasPrev: false,      // show previous button, optional, default: true
      hasNext: false,      // show next button, optional, default: true
      hasClose: true,       // show close button, optional, default: false

      // All icons default to their built-in android equivalents
      // The supplied drawable name, e.g. 'media_play', is the name of a drawable found under android/res/drawable* folders
      // playIcon: 'media_play',
      // pauseIcon: 'media_pause',
      // prevIcon: 'media_prev',
      // nextIcon: 'media_next',
      // closeIcon: 'media_close',
      // notificationIcon: 'notification'
    });
    this.musicControls.subscribe().subscribe(action => {
      console.log(action);
      const message = JSON.parse(action).message;
      console.log(message)
      switch (message) {
        case 'music-controls-next': {
          // Do something
          break;
        }
        case 'music-controls-previous': {
          // Do something
          break;
        }
        case 'music-controls-pause': {
          // Do something
          self.file.pause();
          self.dataService.setPlaying(false)
          this.musicControls.updateIsPlaying(false);
          break;
        }
        case 'music-controls-play': {
          self.file.play();
          self.dataService.setPlaying(true)
          this.musicControls.updateIsPlaying(true);
          // Do something
          break;

        }
        case 'music-controls-destroy':
          // Do something
          break;
        // External controls (iOS only)
        case 'music-controls-toggle-play-pause': {
          // Do something
          break;
        }
        case 'music-controls-seek-to': {
          const seekToInSeconds = JSON.parse(action).position;
          this.musicControls.updateElapsed({
            elapsed: seekToInSeconds,
            isPlaying: true
          });
          // Do something
          break;
        }
        case 'music-controls-skip-forward': {
          // Do something
          break;
        }
        case 'music-controls-skip-backward': {
          // Do something
          break;
        }
      }
      console.log('LISTEN')
    })
    this.musicControls.listen();
    this.musicControls.updateIsPlaying(true);
  }

  async nextSong() {
    // console.log(this.dataService.getSongsToPlay())
    this.songs = this.dataService.getSongsToPlay();
    this.file.release();
    this.dataService.setPlaying(false)
    this.playingSong = false;
    this.musicControls.updateIsPlaying(false)
    clearInterval(this.get_position_interval);
    this.dataService.position = 0;
    let index: number = this.songs.findIndex(element => element.name === this.dataService.getPlayingName())
    if (index != -1) {
      if (index < this.songs.length - 1) {
        index++
      }
      else {
        index = 0
      }
      this.songToPlay = this.songs[index];
      await this.preparePlatform()
      if (this.downloaded) {
        console.log('descargada')
        if (this.platform.is('ios')) {  //ios
          this.file = this.media.create((this.storageDirectory).replace(/^file:\/\//, '') + '/' + this.songToPlay.name + ".mp3");
        } else {  // android
          this.file = this.media.create(this.storageDirectory + this.songToPlay.name + ".mp3");
        }
      }
      else {
        this.file = this.media.create(this.songToPlay.audio);
      }
      this.dataService.getPlayingSong().release();
      this.dataService.setPlayingSong(this.file);
      this.dataService.setPlayingName(this.songToPlay.name)
      this.playingSong = true;
      this.file.play();
      this.dataService.setPlaying(true);
      this.dataService.position = 0;
      this.setInterval();
    }
  }


  async previousSong() {
    this.songs = this.dataService.getSongsToPlay();
    this.file = this.dataService.getPlayingSong();
    this.file.release();
    this.dataService.setPlaying(false)
    this.playingSong = false;
    this.musicControls.updateIsPlaying(false)
    clearInterval(this.get_position_interval);
    this.dataService.position = 0;
    let index: number = this.songs.findIndex(element => element.name === this.dataService.getPlayingName())
    console.log("index 1 =>", index)
    if (index != -1) {
      if (index > 0) {
        index--
      }
      else {
        index = this.songs.length - 1;
      }
      console.log("index 2 =>", index)
      this.songToPlay = this.songs[index];
      await this.preparePlatform()
      if (this.downloaded) {
        console.log('descargada')
        if (this.platform.is('ios')) {  //ios
          this.file = this.media.create((this.storageDirectory).replace(/^file:\/\//, '') + '/' + this.songToPlay.name + ".mp3");
        } else {  // android
          this.file = this.media.create(this.storageDirectory + this.songToPlay.name + ".mp3");
        }
      }
      else {
        this.file = this.media.create(this.songToPlay.audio);
      }
      this.dataService.getPlayingSong().release();
      this.dataService.setPlayingSong(this.file);
      this.dataService.setPlayingName(this.songToPlay.name)
      this.playingSong = true;
      this.file.play();
      this.dataService.setPlaying(true)
      this.dataService.position = 0;
      this.setInterval();
    }
  }

  toggleFavorites() {
    this.songListComponent.toggleFavorite();
  }

  changeVolume() {
    this.file.setVolume(+this.volume / 100)
  }

  get currentPosition() {
    if (this.dataService.position > 0) {
      this.dataService.setShowLoading(false);
    }
    return Math.ceil(this.dataService.position);
  }

  set currentPosition(value) {
    this.dataService.position = value;
  }


  play() {
    console.log('here')
    this.dataService.getPlayingSong().play()
    this.dataService.setPlaying(true)
    this.musicControls.updateIsPlaying(true);
  }

  pause() {
    this.dataService.getPlayingSong().pause();
    this.dataService.setPlaying(false)
    this.musicControls.updateIsPlaying(false);
  }

  goToMediaPlayer() {
    console.log(this.dataService.getSongToPlay())
    if (this.dataService.getFirstTime()) {
      if (!this.dataService.getMediaPlayer()) {
        this.events.publish('goToMediaPlayer');
      }
    }
  }


  unsubscribeEvents() {
    console.log('unsubscribe!')
    this.events.unsubscribe('playMusic')
    this.events.unsubscribe('play')
    this.events.unsubscribe('pause')
    this.events.unsubscribe('next')
    this.events.unsubscribe('previous')
    this.events.unsubscribe('changeLyrics')
  }

  get totalPosition() {
    return this.dataService.position * 1000 + '/' + this.dataService.getDuration() * 1000;
  }
}
