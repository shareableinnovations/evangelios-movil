import { DataService } from './../../services/dataService';
import { Component, Input } from "@angular/core";
import { NavController } from "ionic-angular/navigation/nav-controller";
import { NavParams } from "ionic-angular/navigation/nav-params";
import { BaseComponent } from "../base.component";
import { ToastController, LoadingController, Platform } from "ionic-angular";
import { File } from '@ionic-native/file';

@Component({
    selector: 'card-song',
    templateUrl: 'card-song.component.html'
})

export class CardSongComponent extends BaseComponent {
    @Input('song') song: any;
    album: any;
    categorias: string = '';
    allDownloaded: boolean = false;
    storageDirectory: any;
    showIcon: boolean = false;
    interval: number;
    constructor(protected toastCtrl: ToastController, private navCtrl: NavController, private platform: Platform,
        private navParams: NavParams, private file: File, protected loadingController: LoadingController, private dataService: DataService) {
        super(toastCtrl, loadingController)
      this.album = this.navParams.get('album');
    }

    ngOnInit() {
        const self = this;
        this.platform.ready().then(() => {
            if (this.platform.is('ios')) {
                this.storageDirectory = this.file.dataDirectory;
            } else if (this.platform.is('android')) {
                this.storageDirectory = this.file.externalDataDirectory;
            }
            this.checkDownloadedSong();
        });

        /*console.log('song',this.song);
        console.log('album',this.album);*/
        this.song.categories.forEach(element => {
            if (this.categorias === '') {
                this.categorias = element.name
            } else {
                this.categorias = this.categorias + ', ' + element.name
            }
        });
    }

    async checkDownloadedSong() {
        const self = this;
        await this.platform.ready().then(async () => {
            self.interval = setInterval(async function () {
                await self.file.resolveDirectoryUrl(self.storageDirectory).then(async (resolvedDirectory) => {
                    await self.file.checkFile(resolvedDirectory.nativeURL, self.song.name + ".mp3").then((data) => {
                      if (data == true) {  // exist
                        console.log(2)
                        self.allDownloaded = true;
                        let downloads: any = JSON.parse(localStorage.getItem('downloads'));
                        if(downloads == null) {
                          //
                        } else {

                          console.log('downloads', self.album);
                          let index = downloads.findIndex(element => element.album === self.album.name);

                          if (index !== -1) {
                            if (!downloads[index].selected) {
                              self.file.removeFile(resolvedDirectory.nativeURL, self.song.name + ".mp3").then((data) => {

                                if (data) {  // exist
                                  self.allDownloaded = false;
                                  self.showIcon = false;
                                  throw {code: 1, message: "DELETE FILE"};
                                }

                              }).catch(async err => {
                              });
                            }
                          }
                        }
                      } else {  // not sure if File plugin will return false. go to download
                          self.allDownloaded = false;
                          throw { code: 1, message: "NOT_FOUND_ERR" };
                      }
                    }).catch(async err => {
                    });
                });
                if (self.allDownloaded) {
                    console.log(4)
                    self.showIcon = true;
                    clearInterval(self.interval);
                } else {
                  self.showIcon = false;
                }
            }, 1000);
        })
    }
}
