import { ToastController, LoadingController, NavController, Events } from "ionic-angular";
import { Component } from "@angular/core";
import { BaseComponent } from "../base.component";
import { DataService } from "../../services/dataService";

@Component({
  selector: 'mini-player-component',
  templateUrl: 'miniPlayer.component.html'
})
export class MiniPlayerComponent extends BaseComponent {

  constructor(protected toastCtrl: ToastController, private navCtrl: NavController, protected loadingController: LoadingController,
              private dataService: DataService, private events: Events) {
    super(toastCtrl, loadingController)
  }

  playMusic() {
    this.events.publish('play')
  }

  pauseMusic() {
    this.events.publish('pause')
  }

  nextSong() {
    this.events.publish('next')
  }

  previousSong() {
    this.events.publish('previous')
  }


}
