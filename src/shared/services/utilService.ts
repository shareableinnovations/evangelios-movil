import {NativePageTransitions, NativeTransitionOptions} from "@ionic-native/native-page-transitions";
import {App} from "ionic-angular";
import {Injectable} from "@angular/core";

@Injectable()
export class UtilService {

  nav: any;

  constructor(private app: App,
              private nativePageTransitions: NativePageTransitions) {
  }

  goToPage(page, type, direction?) {
    this.nav = this.app.getActiveNav();
    /*let options: NativeTransitionOptions = {
      direction: direction && direction || 'left',
      duration: 300,
      slowdownfactor: -1,
      iosdelay: 50
    };
    this.nativePageTransitions.slide(options);*/
    let options: NativeTransitionOptions = {
      direction: 'up',
      duration: 500,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 60
    };
    this.nativePageTransitions.fade(options)
      .then(onSuccess=>{})
      .catch(onError=>{});
    if (type === 'push') this.nav.push(page,{},{animate: false});
    else this.nav.setRoot(page, {},{animate: false});
  }

  goToPageParams(page, type, params?) {
    this.nav = this.app.getActiveNav();
    /*let options: NativeTransitionOptions = {
      direction: direction && direction || 'left',
      duration: 300,
      slowdownfactor: -1,
      iosdelay: 50
    };
    this.nativePageTransitions.slide(options);*/
    let options: NativeTransitionOptions = {
      direction: 'up',
      duration: 500,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 60
    };
    this.nativePageTransitions.fade(options)
      .then(onSuccess=>{})
      .catch(onError=>{});
    if (type === 'push') this.nav.push(page,params,{animate: false});
    else this.nav.setRoot(page, {params},{animate: false});
  }

  popPage(direction?) {
    this.nav = this.app.getActiveNav();
    /*let options: NativeTransitionOptions = {
      direction: direction && direction || 'left',
      duration: 300,
      slowdownfactor: -1,
      iosdelay: 50
    };
    this.nativePageTransitions.slide(options);*/
    let options: NativeTransitionOptions = {
      direction: 'up',
      duration: 500,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 60
    };
    this.nativePageTransitions.fade(options)
      .then(onSuccess=>{})
      .catch(onError=>{});
    this.nav.pop({animate: false});
  }
}
