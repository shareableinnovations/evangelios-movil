import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { BaseService } from './BaseService';
import { HttpService } from "./httpService";
import { Environment, Path } from "../../app/const/Const";

@Injectable()
export class UserService extends BaseService {
    constructor(http: HttpService) {
        super(http);

    }

    login(user: any): Promise<any> {
        console.log(JSON.stringify(user));
        return this.http.get(Environment.apiUrl + Path.AUTHENTICATION + '?username=' + user.username + '&password=' + user.pwd).toPromise()
            .then(res => {
                const data = this.extractData(res);
                return data;

            },
            error => {
                return this.handleError(error);

            });

    }

    register(user: any): Promise<any> {
        console.log(JSON.stringify(user));
        return this.http.post(Environment.apiUrl + Path.USER, user).toPromise()
            .then(res => {
                const data = this.extractData(res);
                return data;

            },
            error => {
                return this.handleError(error);

            });
    }

    update(user: any): Promise<any> {
        console.log(JSON.stringify(user));
        return this.http.put(Environment.apiUrl + Path.FAVORITES, user).toPromise()
            .then(res => {
                const data = this.extractData(res);
                return data;

            },
            error => {
                return this.handleError(error);

            });
    }

    updateUser(user: any) : Promise<any>{
        console.log(JSON.stringify(user));
        return this.http.put(Environment.apiUrl + Path.USER, user).toPromise()
            .then(res => {
                const data = this.extractData(res);
                return data;

            },
            error => {
                return this.handleError(error);

            });
    }

    recoverPassword(email:any){
        return this.http.put(Environment.apiUrl + Path.USER + 'recover', {'email': email}).toPromise()
            .then(res => {
                const data = this.extractData(res);
                return data;

            },
            error => {
                return this.handleError(error);

            });
    }
}