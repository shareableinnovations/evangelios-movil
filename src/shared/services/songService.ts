import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/toPromise';
import {BaseService} from './BaseService';
import {HttpService} from "./httpService";
import {Environment, Path} from "../../app/const/Const";

@Injectable()
export class SongService extends BaseService {
  constructor(http: HttpService) {
    super(http);

  }

  getAllSongs(): Promise<any> {
    return this.http.get(Environment.apiUrl + Path.SONG).toPromise()
      .then(res => {
          const data = this.extractData(res);
          return data;
        },
        error => {
          return this.handleError(error);
        });
  }

  getAllSongsAlbum(id: any): Promise<any> {
    // console.log(JSON.stringify(id));
    return this.http.get(Environment.apiUrl + Path.SONG + `/${id}/album`).toPromise()
      .then(res => {
          const data = this.extractData(res);
          return data;
        },
        error => {
          return this.handleError(error);

        });

  }

  getAllSongsAlbumCount(id: any): Promise<any> {
    // console.log(JSON.stringify(id));
    return this.http.get(Environment.apiUrl + Path.SONG + `/${id}/album/count`).toPromise()
      .then(res => {
          const data = this.extractData(res);
          return data;
        },
        error => {
          return this.handleError(error);

        });

  }
}
