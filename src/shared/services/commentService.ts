import { BaseService } from "./BaseService";
import { Injectable } from "@angular/core";
import { HttpService } from "./httpService";
import { Environment, Path } from "../../app/const/Const";

@Injectable()
export class CommentService extends BaseService {
    constructor(http: HttpService) {
        super(http);

    }

    getComments(): Promise<any> {
        return this.http.get(Environment.apiUrl + Path.COMMENT).toPromise()
            .then(res => {
                const data = this.extractData(res);
                return Promise.resolve(data);

            },
            error => {
                return this.handleError(error);

            });
    }

    sendComment(comment:any): Promise<any>{
        return this.http.post(Environment.apiUrl + Path.COMMENT, comment).toPromise()
            .then(res => {
                const data = this.extractData(res);
                return data;
            },
            error => {
                return this.handleError(error);
            });
    }
}