import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/finally';
import { Headers, Http, Request, RequestOptions, RequestOptionsArgs, Response, XHRBackend } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Environment } from '../../app/const/Const';
import { DataService } from "./dataService";
//import { NavController } from 'ionic-angular';




@Injectable()
/**
 * ExtensiÃ³n personalizada de la clase HTTP
 * Permite la configuraciÃ³n de todas las peticiones
 * Captura los envÃ­os y respuestas
 * */
export class HttpService extends Http {
    /** Las direcciones base deberÃ­an venir de la configuraciÃ³n del environment*/
    public apiProxyUrl = Environment.apiUrl;
    private authorization = '';

    constructor(
        backend: XHRBackend,
        defaultOptions: RequestOptions,
        protected dataService: DataService
    ) {
        super(backend, defaultOptions);
        //this.subscribeToToken();
    }

    /**
     * Reescribe el mÃ©todo de la clase base, ejecutando acciones para cada peticiÃ³n
     * La peticiÃ­Ã³n en curso puede llegar como una ruta o una clase request
     * Si viene sÃ³lo la cadena, deberÃ­a traer las opciones aparte
     * */
    request(request: string | Request, options: RequestOptionsArgs = { headers: new Headers() }): Observable<Response> {
        this.configureRequest(request, options);
        return this.interceptResponse(request, options);
    }

    private configureRequest(request: string | Request, options: RequestOptionsArgs) {
        this.setHeaders(request);

    }

    private interceptResponse(request: string | Request, options: RequestOptionsArgs): Observable<Response> {
        const observableRequest = super
            .request(request, options)
            .catch(this.onCatch())
            .finally(this.onFinally());
        return observableRequest;
    }



    /**
     * Interceptor para componer las cabeceras en cada peticiÃ³n
     * */
    private setHeaders(objectToSetHeadersTo: Request | RequestOptionsArgs) {
        const headers = objectToSetHeadersTo.headers;
        headers.set('Content-Type', 'application/json');
        headers.set('Accept-Language', 'es');
        if (this.dataService.getToken()) {
            headers.set('Authorization', this.dataService.getToken());
        }
    }

    /**
     * Interceptor para captura genÃ©rica de errores http
     * */
    private onCatch() {
        return (res: Response) => {
            // Security errors
            /*if (res.status === 401 || res.status === 403) {
              // redirigir al usuario para pedir credenciales
              this.navCtrl.setRoot(HomePage)
            }*/
            // To Do: GestiÃ³n comÃºn de otros errores...
            return Observable.throw(res);
        };
    }

    private onFinally() {
        return () => console.log('Fin');
    }

}