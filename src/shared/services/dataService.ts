import { Injectable } from '@angular/core';
import { User } from "../model/user";
import { Storage } from '@ionic/storage';

@Injectable()
export class DataService {
  private avatar: any;
  private userLoged: User;
  private token: any;
  private songs: any;
  private songsCustom: any;
  songsCount: any;
  private playing: boolean = false;
  private playing_song: any;
  private playing_name: any = '';
  private component: any;
  private duration: any = 0;
  position: any = 0;
  private fromAnotherSong: boolean = false;
  private swipe: boolean = false;
  private remember: boolean;
  private autoLog: boolean;
  private miniPlayer: boolean = false;
  private playerComponent: any;
  private songsToPlay: any;
  private pseudoPlayer: boolean = false
  private songToPlay: any;
  private showLoading: boolean = false;
  private mediaPlayer: boolean = false;
  private listingEvents: boolean = false;
  private timer: number;
  private timerReload: number;
  private timerReloadTemporizador: number;
  private album: any;
  private firstTime: boolean = false;

  constructor(private storage: Storage) {

  }

  getToken() {
    return this.token;
  }

  setToken(value) {
    this.token = value;
  }

  // async getAvatar(): Promise<any>  {
  //   // return this.avatar;
  //   const response = await this.storage.get('avatar');
  //   return response;
  // }
  //
  // async setAvatar(value: any) {
  //   // localStorage.setItem('avatar', value);
  //   // this.avatar = value;
  //   await this.storage.set('avatar', value);
  // }


  async setAvatarCustom(AvatarCustom: any) {
    this.setAvatar(AvatarCustom);
    await this.storage.set('AvatarCustom', AvatarCustom);
  }

  async getAvatarCustom(): Promise<any> {
    const response = await this.storage.get('AvatarCustom');
    return response;
  }

  getAvatar() {
    return this.avatar;
  }

  setAvatar(value) {
    localStorage.setItem('avatar', value);
    this.avatar = value;
  }

  getUserLoged() {
    return this.userLoged;
  }

  setUserLoged(value) {
    this.userLoged = value;
  }

  async setSongsCustom(songsCustom: any) {
    await this.storage.set('songsCustom', songsCustom);
  }

  async getSongsCustom(): Promise<any> {
    const response = await this.storage.get('songsCustom');
    return response;
  }

  setSongs(value) {
    this.songs = value;
  }

  getSongs() {
    return this.songs;
  }

  setSongsCount(value) {
    this.songsCount = value;
  }

  getSongsCount() {
    return this.songsCount;
  }

  getPlaying() {
    return this.playing;
  }

  setPlaying(value) {
    this.playing = value;
  }

  getPlayingSong() {
    return this.playing_song
  }

  setPlayingSong(value) {
    this.playing_song = value;
  }

  getPlayingName() {
    return this.playing_name
  }

  setPlayingName(value) {
    this.playing_name = value;
  }

  setComponent(value) {
    this.component = value
  }

  getComponent() {
    return this.component;
  }

  getSwipe() {
    return this.swipe;
  }

  setSwipe(value) {
    this.swipe = value;
  }

  setDuration(value) {
    this.duration = value;
  }

  getDuration() {
    return this.duration;
  }

  setPosition(value) {
    this.position = value;
  }

  getPosition() {
    return this.position;
  }

  getfromAnotherSong() {
    return this.fromAnotherSong;
  }

  setfromAnotherSong(value) {
    this.fromAnotherSong = value;
  }

  getRemember(): boolean {
    if (localStorage.getItem('remember') === 'true')
      return true;
    else {
      return false;
    }
  }

  setRemember(value) {
    return localStorage.setItem('remember', value);
  }

  getAutoLog() {
    return this.autoLog;
  }

  setAutoLog(value) {
    this.autoLog = value;
  }

  getMiniPlayer() {
    return this.miniPlayer;
  }

  setMiniPlayer(value) {
    this.miniPlayer = value;
  }

  getPlayerComponent() {
    return this.playerComponent;
  }

  setPlayerComponent(value) {
    this.playerComponent = value;
  }

  getAlbum() {
    return this.album;
  }

  setAlbum(value) {
    this.album = value;
  }

  getSongsToPlay() {
    return this.songsToPlay;
  }

  setSongsToPlay(value) {
    this.songsToPlay = value;
  }

  getPseudoPlayer() {
    return this.pseudoPlayer;
  }

  setPseudoPlayer(value) {
    this.pseudoPlayer = value;
  }

  getSongToPlay() {
    return this.songToPlay;
  }

  setSongToPlay(value) {
    this.songToPlay = value;
  }

  getShowLoading() {
    return this.showLoading;
  }

  setShowLoading(value) {
    this.showLoading = value
  }

  getMediaPlayer() {
    return this.mediaPlayer;
  }

  setMediaPlayer(value) {
    this.mediaPlayer = value;
  }

  getListeningEvents() {
    return this.listingEvents
  }

  setListeningEvents(value) {
    this.listingEvents = value;
  }

  setTimer(value) {
    this.timer = value
  }

  getTimer() {
    return this.timer;
  }

  setTimerReload(value) {
    this.timerReload = value
  }

  getTimerReload() {
    return this.timerReload;
  }

  setTimerTemporizador(value) {
    this.timerReloadTemporizador = value
  }

  getTimerTemporizador() {
    return this.timerReloadTemporizador;
  }

  getLocalSongs(): any {
    const local = localStorage.getItem('localSongs');
    if (!local) {
      return [];
    } else {
      return JSON.parse(local);
    }
  }

  setLocalSongs(value) {
    localStorage.setItem('localSongs', JSON.stringify(value));
  }

  getFirstTime(){
    return this.firstTime;
  }

  setFirstTime(value){
    this.firstTime = value;
  }
}
