import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { HttpService } from "./httpService";



@Injectable()
export class BaseService {
    constructor(protected http: HttpService) {
    }


    /**
     * Extract the data of the response of the services
     * @param res service's response
     * @returns {any}
     */
    extractData(res: any) {
        const data = res.json();
        return data;
    }

    // MANEJO DE ERRORES DE LOS SERVICIOS
    handleError(error: any): any {
        const data = this.extractData(error);
        data.mensaje = 'Lo sentimos, en estos momentos no podemos atender sus solicitudes';
        return data;
    }

}