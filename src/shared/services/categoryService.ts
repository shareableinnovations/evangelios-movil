import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { BaseService } from './BaseService';
import { Environment, Path } from "../../app/const/Const";
import { HttpHeaders } from '@angular/common/http';
import { HttpService } from "./httpService";

@Injectable()
export class CategoryService extends BaseService {
    constructor(http: HttpService) {
        super(http);

    }

    getCategories(): Promise<any> {
        return this.http.get(Environment.apiUrl + Path.CATEGORY).toPromise()
            .then(res => {
                const data = this.extractData(res);
                return data;

            },
            error => {
                return this.handleError(error);
            });

    }
}