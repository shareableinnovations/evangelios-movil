export class Comments {

    user: String;
    comment: String;
    email: String;
    date: Date;

    constructor(data?){
        this.user = data && data.user || '';
        this.comment = data && data.comment || null;
        this.email = data && data.email || false;
        this.date = data && data.date || null;
    }
}