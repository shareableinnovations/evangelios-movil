export class User{

    firstName: string;
    lastName: string;
    email: string;
    username: string;
    password: string;
    favorites = [];
    type: number;
    role: string;
    image: string;
    constructor(data?){
        this.firstName = data && data.name || '';
        this.lastName = data && data.lastName || '';
        this.email = data && data.email || '';
        this.username = data && data.username || '';
        this.password = data && data.password || '';
        this.favorites = [];
        this.type = 1;
        this.role = 'Usuario'
        this.image = data && data.image || '';
    }


}