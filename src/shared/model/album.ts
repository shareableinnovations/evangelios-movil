export class Album {

    id: string;
    image: any;
    isActive: boolean;
    lastModify: string;
    name: string;

    constructor( data? ){
        this.id = data && data.id || '';
        this.image = data && data.image || null;
        this.isActive = data && data.isActive || false;
        this.lastModify = data && data.lastModify || '';
        this.name = data && data.name || '';
    }
}