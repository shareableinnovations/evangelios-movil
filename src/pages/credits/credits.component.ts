import {DataService} from './../../shared/services/dataService';
import {BaseComponent} from "../../shared/components/base.component";
import {Component, ElementRef, OnInit, ViewChild} from "@angular/core";
import {NavController} from "ionic-angular/navigation/nav-controller";
import {HomePage} from "../home/home";
import {ToastController, LoadingController, Content} from "ionic-angular";

@Component({
  selector: 'page-credits',
  templateUrl: 'credits.component.html'
})
export class CreditsPage extends BaseComponent implements OnInit {

  @ViewChild(Content) content: Content;
  setIntervalCustom: any;

  constructor(protected toastCtrl: ToastController, private navCtrl: NavController, protected loadingController: LoadingController,
              private dataService: DataService) {
    super(toastCtrl, loadingController)
  }


  onScroll(event: any) {
    // console.log(event);
  }

  ngOnInit() {
    // this.scrollAutomatic();
  }

  scrollAutomatic(scroll?: any) {
    // setTimeout(() => {
    //   let timeout = 0;
    //   this.setIntervalCustom = setInterval(() => {
    //     // console.log(this.content.scrollTop);
    //     // console.log(this.content._elementRef.nativeElement.clientHeight, 'clientHeight');
    //     // console.log(this.content._elementRef.nativeElement.scrollHeight, 'scrollHeight');
    //     const b = (this.content._elementRef.nativeElement.scrollHeight / 100) + timeout;
    //     // console.log(b);
    //     this.content.scrollTo(0, b);
    //     timeout++;
    //   }, 200);
    // }, 2000);
  }

}
