import {ImagePicker} from '@ionic-native/image-picker';
import {BaseComponent} from "../../shared/components/base.component";
import {Component} from "@angular/core";
import {MenuController, NavController, ToastController, LoadingController, Platform} from "ionic-angular";
import {UserService} from "../../shared/services/userService";
import {LoginPage} from "../login/login.component";
import {User} from "../../shared/model/user";
import {PlayListPage} from "../playLists/playList.component";
import {DataService} from "../../shared/services/dataService";
import {AndroidPermissions} from '@ionic-native/android-permissions';
import {Camera, CameraOptions} from '@ionic-native/camera';
import {HomeDonationPage} from "../homeDonation/homeDonation.component";
import {Album} from "../../shared/model/album";
import {SongService} from "../../shared/services/songService";
import {AlbumService} from "../../shared/services/albumService";

@Component({
  selector: 'page-register',
  templateUrl: 'register.component.html'
})
export class RegisterPage extends BaseComponent {
  user: User;
  avatar: string;
  confirmPassword: string = '';
  options: any;
  full: boolean = false;
    items: Array<Album> = [];
    itemsCount: Array<any> = [];

  constructor(private dataService: DataService,
              public platform: Platform,
              protected toastCtrl: ToastController,
              public menuController: MenuController,
              public navController: NavController,
              private albumService: AlbumService,
              private userService: UserService,
              protected loadingController: LoadingController,
              private imagePicker: ImagePicker,
              private androidPermissions: AndroidPermissions,
              private camera: Camera,
              private songService: SongService) {

    super(toastCtrl, loadingController);
    this.user = new User();
    this.avatar = 'assets/img/avatars/noavatar.png'
    this.menuController.swipeEnable(false);
    const self = this;
    this.platform.ready().then(() => {
      if (this.platform.is('ios')) {

      } else if (this.platform.is('android')) {
        console.log('entre')
        self.options = {maximumImagesCount: 1};
        self.checkPermissions()
      }
    });
  }

  goToLogin() {
    this.navController.push(LoginPage, {}, {animate: false})
  }

  register() {
    this.user.username = this.user.username.toLowerCase()
    this.user.email = this.user.email.toLocaleLowerCase();
    this.user.username = this.user.email;
    this.user.image = this.avatar;
    if (this.user.password === this.confirmPassword) {
      this.presentLoadingDefault()
      this.userService.register(this.user).then(result => {
        console.log(result)
        if (result.error) {
          this.dismissLoader()
          this.presentToastCenter(result.error)
        } else {
          this.dismissLoader()
          console.log(result);
          this.presentToastCenter("Usuario Registrado satisfactoriamente")
          localStorage.setItem('userRemembered', '')
          // this.navController.setRoot(LoginPage)
            this.explorarLogin(this.user.username, this.user.password)
        }
      }).catch(error => this.presentToastCenter(error))
    } else {
      this.presentToastCenter('Las claves deben ser iguales')
    }
  }

  explorarLogin(user, password) {
    this.presentLoadingDefault();
    this.userService.login({'username': user, 'pwd': password}).then(async (result) => {
      if (result.error) {
        throw new Error('por favor verifica tus datos e intenta nuevamente')
      } else {
        this.dataService.setToken(result.data.token);
        this.dataService.setUserLoged(result.data.user);
        await this.dataService.setAvatarCustom(result.data.user.image && result.data.user.image || '../assets/img/avatars/noavatar.png');
        this.dataService.setRemember(true);
        localStorage.setItem('userRemembered', JSON.stringify(result.data.user))
        this.navController.setRoot(HomeDonationPage, {}, {animate: false});
        this.dismissLoader()
        /*if (this.remember) {
          localStorage.setItem('userRemembered', JSON.stringify(result.data.user))
        }
        else {
          localStorage.setItem('userRemembered', '');
        }*/
      }
    })
      .then(async () => await this.getSongs())
      .then(() => this.getAlbums())
      .catch(error => {
        console.log(error.toString())

        let err: string = error.toString();

        // console.log(err.toLowerCase)
        if (err === 'Error: por favor verifica tus datos e intenta nuevamente') {
          this.presentToastCenter("Credenciales Invalidas")
        } else {
          if (err === "TypeError: Cannot read property 'token' of undefined") {
            this.presentToastCenter("error: verifique su conexión a internet")
          } else {
            this.presentToastCenter(error)
          }
        }

        this.dismissLoader()
      })
  }

  getAlbums() {
    this.albumService.getAlbums().then(result => {
      if (result.error) {
        throw new Error(result.error);
      } else {
        result.data.albums.forEach(element => {
          let album = new Album(element);
          this.items.push(album);

          this.songService.getAllSongsAlbumCount(album.id).then(result => {
            if (result.error) {
              throw new Error(result.error);
            } else {
              console.log(result.data);
              this.itemsCount.push(result.data);
              localStorage.setItem('songsCount', JSON.stringify(this.itemsCount));
            }
          })
        });
        localStorage.setItem('albums', JSON.stringify(this.items));
        this.navController.setRoot(HomeDonationPage, {}, {animate: false});
        this.dismissLoader()
      }
    })
  }

  async getSongs() {
    await this.songService.getAllSongs().then(result => {
      if (result.error) {
        throw new Error(result.error);
      } else {
        // this.dataService.setSwipe(true);
        this.dataService.setSongsCustom(result.data);
        // this.navController.setRoot(HomeDonationPage, {}, { animate: false });
      }
    })
  }

  pickImage() {
    this.takePhoto(0);
  }


  takePhoto(sourceType: number) {
    const self = this;
    const options: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      sourceType: sourceType,
    }

    this.camera.getPicture(options).then((imageData) => {
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      self.avatar = base64Image;
    }, (err) => {
      // Handle error
    });
  }


  checkPermissions() {
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE)
      .then(result => {
        if (result.hasPermission === false) {
          this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE])
        }
      })
  }

  checkParameters() {
    this.full = false;
    if (this.user.firstName != '' && this.user.password != '' && this.user.firstName != '' &&
      this.user.lastName != '' && this.user.password != '' && this.confirmPassword != '' &&
      this.validateEmail()) {
      this.full = true;
    }
  }

  validateEmail() {
    var re = /\S+@\S+\.\S+/;
    return re.test(this.user.email);
  }
}
