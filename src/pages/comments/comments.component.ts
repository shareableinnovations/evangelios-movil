import { DataService } from './../../shared/services/dataService';
import { CommentService } from './../../shared/services/commentService';
import { BaseComponent } from '../../shared/components/base.component';
import { Component } from "@angular/core";
import { LoadingController, ToastController, Loading } from "ionic-angular";
import { Comments } from './../../shared/model/comment';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
@Component({
  selector: 'page-comments',
  templateUrl: 'comments.component.html'
})
export class CommentsPage extends BaseComponent {
  comment: Comments;
  loading: Loading;
  category: any = 'Sugerencias';

  constructor(protected toastCtrl: ToastController,
    protected loadingController: LoadingController,
    private commentService: CommentService,
    private alertCtrl: AlertController) {
    super(toastCtrl, loadingController);
    this.comment = new Comments();
  }

  async submitComment() {
    console.log(JSON.parse(localStorage.getItem('userRemembered')))
    this.comment.user = JSON.parse(localStorage.getItem('userRemembered'))._id;
    this.comment.date = new Date();
    this.comment.email = JSON.parse(localStorage.getItem('userRemembered')).email;
    this.presentLoadingDefault();
    try {
      this.comment.comment = `${this.category}: ${this.comment.comment}`;
      // console.log(this.comment)
      // console.log(this.category)
      const res = await this.commentService.sendComment(this.comment)
      // // console.log(res);
      this.comment = new Comments();
      this.dismissLoader()
      this.showAlert();
    }
    catch (error) {
      console.log(error);
      this.presentToastCenter(error)
      this.dismissLoader()
    }
  }

  showAlert() {
    const alert = this.alertCtrl.create({
      title: 'Comentarios',
      subTitle: 'Su comentario se ha enviado satisfactoriamente',
      buttons: ['OK']
    });
    alert.present();
  }

}
