import { Component } from '@angular/core';
import {IonicPage, LoadingController, MenuController, NavController, NavParams, ToastController} from 'ionic-angular';
import {UtilService} from "../../shared/services/utilService";
import {LoginPage} from "../login/login.component";
import {HomeDonationPage} from "../homeDonation/homeDonation.component";
import {UserService} from "../../shared/services/userService";
import {DataService} from "../../shared/services/dataService";
import {AlbumService} from "../../shared/services/albumService";
import {SongService} from "../../shared/services/songService";
import {BaseComponent} from "../../shared/components/base.component";
import {Album} from "../../shared/model/album";

/**
 * Generated class for the PreviewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-preview',
  templateUrl: 'preview.html',
})
export class PreviewPage extends BaseComponent {
  items: Array<Album> = [];
  itemsCount: Array<any> = [];
  username: string = '';
  pwd: string = '';
  remember: boolean;

  constructor(public navCtrl: NavController,
              protected toastCtrl: ToastController,
              public menuController: MenuController,
              public navController: NavController,
              private userService: UserService,
              private dataService: DataService,
              protected loadingController: LoadingController,
              private albumService: AlbumService,
              private songService: SongService,
              private utilService: UtilService) {

    super(toastCtrl, loadingController)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PreviewPage');
  }

  goLogin() {
    this.utilService.goToPage(LoginPage, 'push');
  }

  explorar() {
    this.presentLoadingDefault();
    this.userService.login({ 'username': 'demo@jesusdenazart.cl', 'pwd': 123456 }).then(async (result) => {
      if (result.error) {
        throw new Error('por favor verifica tus datos e intenta nuevamente')
      }
      else {
        this.dataService.setToken(result.data.token);
        this.dataService.setUserLoged(result.data.user);
        await this.dataService.setAvatarCustom(result.data.user.image && result.data.user.image || '../assets/img/avatars/noavatar.png');
        this.dataService.setRemember(true);
        localStorage.setItem('userRemembered', JSON.stringify(result.data.user))
        this.navController.setRoot(HomeDonationPage, {}, { animate: false });
        this.dismissLoader()
        /*if (this.remember) {
          localStorage.setItem('userRemembered', JSON.stringify(result.data.user))
        }
        else {
          localStorage.setItem('userRemembered', '');
        }*/
      }
    })
      .then(async () => await this.getSongs())
      .then(() => this.getAlbums())
      .catch(error => {
        console.log(error.toString())

        let err:string = error.toString();

        // console.log(err.toLowerCase)
        if (err === 'Error: por favor verifica tus datos e intenta nuevamente') {
          this.presentToastCenter("Credenciales Invalidas")
        } else {
          if (err === "TypeError: Cannot read property 'token' of undefined") {
            this.presentToastCenter("error: verifique su conexión a internet")
          }
          else {
            this.presentToastCenter(error)
          }
        }

        this.dismissLoader()
      })
  }

  async getSongs() {
    await this.songService.getAllSongs().then(result => {
      if (result.error) {
        throw new Error(result.error);
      }
      else {
        // this.dataService.setSwipe(true);
        this.dataService.setSongsCustom(result.data);
        // this.navController.setRoot(HomeDonationPage, {}, { animate: false });
      }
    })
  }

  getAlbums() {
    this.albumService.getAlbums().then(result => {
      if (result.error) {
        throw new Error(result.error);
      }
      else {
        result.data.albums.forEach(element => {
          let album = new Album(element);
          this.items.push(album);

          this.songService.getAllSongsAlbumCount(album.id).then(result => {
            if (result.error) {
              throw new Error(result.error);
            }else {
              console.log(result.data);
              this.itemsCount.push(result.data);
              localStorage.setItem('songsCount', JSON.stringify(this.itemsCount));
            }
          })
        });
        localStorage.setItem('albums', JSON.stringify(this.items));
        this.navController.setRoot(HomeDonationPage, {}, { animate: false });
        this.dismissLoader()
      }
    })
  }

}
