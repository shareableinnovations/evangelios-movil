import { DataService } from './../../shared/services/dataService';
import { UtilService } from "../../shared/services/utilService";
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LoginPage } from "../login/login.component";
import { MenuController } from 'ionic-angular/components/app/menu-controller';
import { RegisterPage } from "../register/register.component";
import {PreviewPage} from "../preview/preview";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController,
              public menuController: MenuController,
              private dataService: DataService,
              private utilService: UtilService) {
    this.menuController.swipeEnable(false);
    this.dataService.setPseudoPlayer(false);
  }

  gotoLogin(){
    // this.navCtrl.push(LoginPage)
    this.utilService.goToPage(PreviewPage, 'push');
    // this.utilService.goToPage(LoginPage, 'push');
  }

  goToRegisterPage(){
    // this.navCtrl.push(RegisterPage)
    this.utilService.goToPage(RegisterPage, 'push');
  }

}
