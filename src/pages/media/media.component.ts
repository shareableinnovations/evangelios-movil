import {Content, NavController, Scroll} from 'ionic-angular';
import {NavParams} from 'ionic-angular/navigation/nav-params';
import {MusicControls} from '@ionic-native/music-controls';
import {DataService} from './../../shared/services/dataService';
import {ToastController, LoadingController, Events, Navbar} from 'ionic-angular';
import {BaseComponent} from './../../shared/components/base.component';
import {Component, ViewChild, ElementRef, QueryList} from '@angular/core';
import {Media, MediaObject} from '@ionic-native/media';
import {Platform} from 'ionic-angular/platform/platform';
import {File} from '@ionic-native/file';
import {FileTransferObject, FileTransfer} from "@ionic-native/file-transfer";
import {NativePageTransitions, NativeTransitionOptions} from "@ionic-native/native-page-transitions";
import {InfoPage} from "../info/info";
import {Network} from "@ionic-native/network";
import {UtilService} from "../../shared/services/utilService";
import {PlayListPage} from "../playLists/playList.component";

@Component({
  selector: 'page-media',
  templateUrl: 'media.component.html'
})
export class MediaComponent extends BaseComponent {
  lyrics: any = [];
  showLoading = false;
  position = 0;
  duration = 0;
  file: MediaObject;
  playingSong: boolean = false;
  get_position_interval: any;
  songs = [];
  pos = 0;
  volume = 50;
  songListComponent: any;
  songToPlay: any;
  downloaded: boolean = false;
  storageDirectory: any;
  _setIntervalLyrics: any;
  _currentPosition = 0;

  @ViewChild(Navbar) navBar: Navbar;
  @ViewChild('scroll') scroll: ElementRef;
  @ViewChild('scrollH') scrollH: QueryList<ElementRef>;

  constructor(protected toastCtrl: ToastController,
              protected loadingController: LoadingController,
              private dataService: DataService, private media: Media,
              private musicControls: MusicControls, private navParams: NavParams, private nativePageTransitions: NativePageTransitions,
              private miniPlayerEvents: Events, private platform: Platform, private savedFile: File, private transfer: FileTransfer,
              private navCtrl: NavController,
              private events: Events,
              private network: Network,
              private utilService: UtilService) {
    super(toastCtrl, loadingController);
  }

  async ngOnInit() {
    this.customEvents()
    this.dataService.setPseudoPlayer(false);
    //console.log(this.dataService.getSongToPlay());
    if (this.dataService.getSongToPlay().lyric.data &&
      this.dataService.getSongToPlay().lyric.data !== undefined &&
      this.dataService.getSongToPlay().lyric.data !== '' &&
      this.dataService.getSongToPlay().lyric.data !== null) {
      this.parseSrtFileToJson(this.dataService.getSongToPlay().lyric.data);
      // this.getInterval();
    }
  }

  customEvents() {
    this.events.subscribe('next', () => {
      console.log('next')
      this.changeSongNext();
    });
    this.events.subscribe('changeLyrics', () => {
      console.log('changeLyrics');
      this.changeSongNext();
    });
    this.events.subscribe('previous', () => {
      console.log('previous');
      this.changeSongPrevius();
    });
  }

  changeSongNext() {
    let songs = this.dataService.getSongs();
    let index: number = this.dataService.getSongs().findIndex(element => element.name === this.dataService.getPlayingName())
    console.log("index 1 =>", index)
    if (index != -1) {
      if (index < songs.length - 1) {
        index++
      }
      else {
        index = 0
      }
      console.log("index 2 =>", index)
      console.log("index  =>", index)
      console.log("song =>", songs)
      this.parseSrtFileToJson(songs[index].lyric.data);
    }
  }

  changeSongPrevius() {
    let songs = this.dataService.getSongs();
    let index: number = this.dataService.getSongs().findIndex(element => element.name === this.dataService.getPlayingName())
    console.log("index 1 =>", index)
    if (index != -1) {
      if (index > 0) {
        index--
      }
      else {
        index = songs.length - 1;
      }
      console.log("index 2 =>", index)
      console.log("index  =>", index)
      console.log("song =>", songs)
      this.parseSrtFileToJson(songs[index].lyric.data);
    }
  }

  getInterval() {
    setTimeout(() => {
      // console.log(this.scroll);
      // let songcount = 0;
      let timerO: any = 0;
      // setInterval songs
      this._setIntervalLyrics = setInterval(() => {
        // console.log(this.scroll, 'elemtn');
        if (this.lyrics.length > 0) {
          if (this.dataService.getPlaying() && !this.dataService.getShowLoading()) {
            let count: any;
            let time: any;
            let lenghtSongs: any = [];

            // calculate active
            if (this.scroll.nativeElement.children && this.scroll.nativeElement.children.length > 0) {
              for (let children in this.scroll.nativeElement.children) {
                // class active
                if (this.scroll.nativeElement.children[children].classList && this.scroll.nativeElement.children[children].classList.value === 'subtitle active') {
                  lenghtSongs.push(this.scroll.nativeElement.children[children]);
                }
              }
            }
            // if exists lyrics
            if (this.lyrics.length > 0 && this.lyrics[lenghtSongs.length - 1] && this.lyrics[lenghtSongs.length - 1].activeIn) {
              // calculate songs
              let totalSongs = this.scroll.nativeElement.scrollHeight / this.lyrics[0].end;
              // calculate songs
              let a = this.lyrics[lenghtSongs.length - 1].activeIn * totalSongs;
              // calculate scroll down
              let b = a - (this.scroll.nativeElement.clientHeight / 2.5);
              // scroll
              this.scroll.nativeElement.scrollTo(0, b);
            }
          }

        }
      }, 100);
    }, 1000);
  }

  ionViewDidLoad() {
    this.setBackButtonAction()
  }

  setBackButtonAction() {
    this.navBar.backButtonClick = () => {
      //Write here wherever you wanna do
      let options: NativeTransitionOptions = {
        direction: 'down',
        duration: 500,
        slowdownfactor: -1,
        iosdelay: 100,
        androiddelay: 150
      };
      this.nativePageTransitions.slide(options)
      // this.navCtrl.pop()
      this.navCtrl.setRoot(PlayListPage);

      // this.utilService.goToPage(PlayListPage, 'root');
    }
  }

  async ngAfterViewInit() {
    let img = new Image();
    img.src = this.dataService.getSongToPlay().image;
    let imgCUstom = img.src;
    let album = this.dataService.getAlbum();
    const data = await this.checkFile(album.name + ".png");
    console.log(data);
    if (data) {
      await this.platform.ready();
      if (this.platform.is('ios')) {
        this.storageDirectory = this.savedFile.dataDirectory;
      }
      if (this.platform.is('android')) {
        this.storageDirectory = this.savedFile.externalDataDirectory;
      }
      console.log('no download image')

      const resolvedDirectory = await this.savedFile.resolveDirectoryUrl(this.storageDirectory);
      imgCUstom = resolvedDirectory.nativeURL.replace(/^file:\/\//, '') + album.name + ".png";
    }
    document.getElementById('headerMedia').style.backgroundImage = "url('" + imgCUstom + "')";
    document.getElementById('headerMedia').style.backgroundSize = 'cover';
    document.getElementById('headerMedia').style.backgroundPosition = 'center';
    document.getElementById('headerMedia').style.boxShadow = "0 0 0 1000px rgba(0,0,0,0.61) inset";
  }

  async checkFile(path) {
    try {
      await this.platform.ready();
      if (this.platform.is('ios')) {
        this.storageDirectory = this.savedFile.dataDirectory;
      }
      if (this.platform.is('android')) {
        this.storageDirectory = this.savedFile.externalDataDirectory;
      }
      const resolvedDirectory = await this.savedFile.resolveDirectoryUrl(this.storageDirectory);
      const data = await this.savedFile.checkFile(resolvedDirectory.nativeURL, path);
      return data;
    } catch (err) {
      if (err.code === 1) {
        return null;
      }
      throw err;
    }
  }

  toggleFavorites() {
    this.songListComponent.toggleFavorite();
  }

  changeVolume() {
    this.dataService.getPlayingSong().setVolume(+this.volume / 100)
  }

  previousSong() {
    this.miniPlayerEvents.publish('previous');
  }

  nextSong() {
    this.miniPlayerEvents.publish('next');
  }

  playMusic() {
    this.miniPlayerEvents.publish('play')
  }

  pauseMusic() {
    this.miniPlayerEvents.publish('pause')
  }

  get currentPosition() {
    if (this.dataService.position > 0) {
      this.showLoading = false;
    }
    return Math.ceil(this.dataService.position);
  }

  set currentPosition(value) {
    this.dataService.position = value;
  }

  ionViewCanLeave() {
    this.dataService.setMediaPlayer(false);
    this.dataService.setPseudoPlayer(true);
  }

  getTimeInSeconds(time: string): number {

    const array = time ? time.split(':') : [];
    if (array.length === 3) {
      const hours = parseInt(array[0]) * 3600;
      const minutes = parseInt(array[1]) * 60;
      const seconds = parseFloat(array[2]);

      return hours + minutes + seconds;
    } else if (array.length === 2) {
      const minutes = parseInt(array[0]) * 60;
      const seconds = parseFloat(array[1]);

      return minutes + seconds;
    }

    return 0;
  }

  parseSrtFileToJson(file: string) {
    this.lyrics = [];
    if (file.length > 0) {
      console.log("Parsing lyrics");
      this.lyrics = [];
      const request = new XMLHttpRequest();
      request.open('GET', file);
      request.onreadystatechange = () => {
        if (request.readyState === 4) {
          const subtitles = request.responseText.split('\n');
          // console.log(subtitles, 'subtitles');
          for (let index = 0; index < subtitles.length; index += 1) {
            // console.log(subtitles[index])
            const newSubtitle = {
              text: subtitles[index],
            };
            this.lyrics.push(newSubtitle);
          }
          // console.log(this.lyrics, 'lyrics');
        }
      };
      request.send();
    }
    }

}
