import { HomeDonationPage } from './../homeDonation/homeDonation.component';
import { Component } from '@angular/core';
import { NavController, ToastController, LoadingController } from 'ionic-angular';
import { BaseComponent } from "../../shared/components/base.component";
import { MenuController } from 'ionic-angular/components/app/menu-controller';
import { PlayListPage } from '../playLists/playList.component';
import { UserService } from "../../shared/services/userService";
import { RegisterPage } from "../register/register.component";
import { DataService } from "../../shared/services/dataService";
import { SongService } from "../../shared/services/songService";

@Component({
    selector: 'page-forgot',
    templateUrl: 'forgotPassword.component.html'
})
export class ForgotPage extends BaseComponent {

    email: string = '';
    full: boolean;
    constructor(protected toastCtrl: ToastController,
        public menuController: MenuController,
        public navController: NavController,
        private userService: UserService,
        private dataService: DataService,
        protected loadingController: LoadingController,
        private songService: SongService) {

        super(toastCtrl, loadingController)
        this.dataService.setSwipe(false);
    }
    //(ngModelChange)="checkParameters()"
    //(click)="recover()"
    checkParameters() {
        if (this.email != '') {
            this.full = true;
        }
        else {
            this.full = false;
        }
    }

    recover() {
        this.presentLoadingDefault();
        this.userService.recoverPassword(this.email).then(result => {
            console.log(result);
            this.dismissLoader();
        })
    }
}