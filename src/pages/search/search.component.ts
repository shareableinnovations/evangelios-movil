import { BaseComponent } from "../../shared/components/base.component";
import {Component, OnInit} from "@angular/core";
import { ToastController, LoadingController, NavController, Events } from "ionic-angular";
import { SongsPage } from "../songList/songList.component";
import { AlbumService } from "../../shared/services/albumService";
import { Album } from "../../shared/model/album";
import { DataService } from "../../shared/services/dataService";
import {HomeDonationPage} from "../homeDonation/homeDonation.component";
import {SongService} from "../../shared/services/songService";
import {MediaComponent} from "../media/media.component";

@Component({
    selector: 'page-searchList',
    templateUrl: 'search.component.html'
})
export class SearchListPage extends BaseComponent{
    items = []
    albums = []
    songs:  any;
    myInput = '';
    classHeader = 'header-song';
    shouldShowCancel = false;
    constructor(protected toastCtrl: ToastController, protected loadingController: LoadingController,
        private navCtrl: NavController, private albumService: AlbumService, private dataService: DataService, private events: Events,
                private songService: SongService) {
        super(toastCtrl, loadingController);
        this.items = [];
        //this.getAlbums();
      this.ngOnInit();
    }

    async ngOnInit() {
      this.songs = await this.dataService.getSongsCustom();
    }

    onInput(event) {
        const val = event.target.value;
        if (val === '') {
            this.items = [];
        }
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.items = this.songs.filter((item) => {
                return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
            })
        }
    }

    onCancel(event) {

    }

    async play(song?) {
      // console.log(song);
        // let listToPass: Array<any> = this.songs.slice();
        // const index = listToPass.findIndex(element => element.name === song.name)
        // this.dataService.setSongsToPlay(listToPass);
        // this.dataService.setSongToPlay(song);
        // this.events.publish('playMusic');
        // this.navCtrl.push(MediaComponent, { song: song, songList: listToPass, componentSongList: this });

      // console.log(song);
      if (!this.dataService.getPseudoPlayer()) {
        await this.dataService.setPseudoPlayer(true);
        this.classHeader = 'header-song-activated';
        // this.play(song);
      }
      setTimeout(()=> {
        let listToPass: Array<any> = this.songs.slice();
        const index = listToPass.findIndex(element => element.name === song.name);
        this.dataService.setSongsToPlay(listToPass);
        this.dataService.setSongToPlay(song);
        this.events.publish('playMusic');
        this.dataService.setTimerReload(0);
      },1500);

    }

    // showAlbum(album) {
    //     this.navCtrl.push(SongsPage, { album: album, type: 0 });
    // }

    // getAlbums() {
    //     this.presentLoadingDefault();
    //     this.albumService.getAlbums().then(result => {
    //         if (result.error) {
    //             this.items = JSON.parse(localStorage.getItem('albums'));
    //             if (this.items && this.items.length > 0) {

    //             }
    //             else {
    //                 this.presentToastCenter(result.error)
    //             }
    //             this.dismissLoader()
    //         }
    //         else {
    //             result.data.albums.forEach(element => {
    //                 let album = new Album(element);
    //                 this.albums.push(album);
    //             });
    //             localStorage.setItem('albums', JSON.stringify(this.items));
    //             this.dismissLoader()
    //         }
    //     })
    // }

}
