import { BaseComponent } from "../../shared/components/base.component";
import { Component } from "@angular/core";
import { ToastController, LoadingController, NavController } from "ionic-angular";
import { CategoryService } from "../../shared/services/categoryService";
import { Album } from "../../shared/model/album";
import { SongsPage } from "../songList/songList.component";
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import {Network} from "@ionic-native/network";

@Component({
    selector: 'page-categoryList',
    templateUrl: 'categoryList.component.html'
})
export class CategoryListPage extends BaseComponent {
    items = []
    constructor(protected toastCtrl: ToastController,
        protected loadingController: LoadingController,
        private categoryService: CategoryService,
        private nativePageTransitions: NativePageTransitions,
        private navCtrl: NavController,
        private network: Network) {
        super(toastCtrl, loadingController)
        this.items = [];
        this.getCategories();
    }

    getCategories() {
        this.presentLoadingDefault();
        if (this.network.type !== 'unknown' && this.network.type !== 'none') {
          this.categoryService.getCategories().then(result => {
            if (result.error) {
              this.items = JSON.parse(localStorage.getItem('categories'));
              if (this.items && this.items.length > 0) {

              }
              else {
                this.presentToastCenter(result.error)
              }
              this.dismissLoader()
            }
            else {
              console.log(result)
              result.data.categories.forEach(element => {
                console.log(element)
                let album = new Album(element);
                this.items.push(album);
              });
              localStorage.setItem('categories', JSON.stringify(this.items));
              this.dismissLoader()
            }
          })
        } else {
          this.toastCtrl.create({
            message: 'No tiene conexión a internet',
            duration: 3000,
            position: 'top'
          }).present();
          this.dismissLoader()
        }
    }
    showAlbum(album) {
      let options: NativeTransitionOptions = {
        direction: 'up',
        duration: 500,
        slowdownfactor: 3,
        slidePixels: 20,
        iosdelay: 100,
        androiddelay: 150,
        fixedPixelsTop: 0,
        fixedPixelsBottom: 60
      };
      this.nativePageTransitions.fade(options)
        .then(onSuccess=>{})
        .catch(onError=>{});
        this.navCtrl.push(SongsPage, { album: album, type: 1 },{animate: false});
    }

}
