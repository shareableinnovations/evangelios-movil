import { BaseComponent } from "../../shared/components/base.component";
import { Component, ViewChild } from "@angular/core";
import { NavController } from "ionic-angular/navigation/nav-controller";
import { NavParams } from "ionic-angular/navigation/nav-params";
import {ToastController, LoadingController, Events, Navbar, Platform} from "ionic-angular";
import { UserService } from "../../shared/services/userService";
import { DataService } from "../../shared/services/dataService";
import {File} from "@ionic-native/file";
import {HomeDonationPage} from "../homeDonation/homeDonation.component";
import {SongService} from "../../shared/services/songService";

@Component({
  selector: 'page-songList',
  templateUrl: 'songList.component.html'
})
export class SongsPage extends BaseComponent {
  @ViewChild(Navbar) navBar: Navbar;
  album: any;
  _timer: any;
  songs: any;
  favorite: boolean;
  index: number;
  classHeader = 'header-song';
  storageDirectory: any;
  timeReload: any;
  constructor(protected toastCtrl: ToastController,
              private navCtrl: NavController,
              private navParams: NavParams,
              protected loadingController: LoadingController,
              private userService: UserService,
              private dataService: DataService,
              private events: Events,
              private platform: Platform,
              private file: File,
              private songService: SongService) {
    super(toastCtrl, loadingController,)
    this.songs = [];
    this.album = this.navParams.get('album');
    this.dataService.setAlbum(this.album);
    //this.dataService.setPseudoPlayer(true)
    this.prepareSongs(this.navParams.get('type'));

    this.platform.ready().then(() => {
      if (this.platform.is('ios')) {
        this.storageDirectory = this.file.dataDirectory;
      } else if (this.platform.is('android')) {
        this.storageDirectory = this.file.externalDataDirectory;
      }
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SongsPage');
    this.navBar.backButtonClick = (e: UIEvent) => {
      document.getElementById('header').style.display = 'none';
      this.navCtrl.pop({animate: false});
    };
  }

  ngAfterViewInit() {
    // console.log(this.album)
    if (this.dataService.getUserLoged().favorites) {
      this.index = this.dataService.getUserLoged().favorites.findIndex(element => element === this.album.id);
      if (this.index === -1) {
        this.favorite = false;
      }
      else {
        this.favorite = true;
      }
    }
    this.putImageBackground();
  }

  async putImageBackground() {
    // let img = new Image();
    // img.src = this.album.image;
    // let imgCUstom =  img.src;
    // const data = await this.checkFile(this.album.name + ".png");
    // console.log(data);
    // if (data) {
    //   await this.platform.ready();
    //   if (this.platform.is('ios')) {
    //     this.storageDirectory = this.file.dataDirectory;
    //   }
    //   if (this.platform.is('android')) {
    //     this.storageDirectory = this.file.externalDataDirectory;
    //   }
    //   console.log('no download image')
    //
    //   const resolvedDirectory = await this.file.resolveDirectoryUrl(this.storageDirectory);
    //   imgCUstom =  resolvedDirectory.nativeURL.replace(/^file:\/\//, '')+this.album.name+ ".png";
    // }
    let img = new Image();
    switch(this.album.id) {
      case '5b3add61b520934ecfd21838':
        img.src = 'assets/imgs/CD1-La-Natividad.jpg';
        break;
      case '5b3ceb94339c8c1ad990a9d2':
        img.src = 'assets/imgs/CD2-Inicios.jpg';
        break;
      case '5b3cebdb339c8c1ad990a9d3':
        img.src = 'assets/imgs/CD3-Parabolas.jpg';
        break;
      case '5b3cebe9339c8c1ad990a9d4':
        img.src = 'assets/imgs/CD4-Milagros.jpg';
        break;
      case '5b3cebfd339c8c1ad990a9d5':
        img.src = 'assets/imgs/CD5-Predicacion.jpg';
        break;
      case '5b3cec15339c8c1ad990a9d6':
        img.src = 'assets/imgs/CD6-jesus-hijo-de-dios-i.jpg';
        break;
      case '5b3cec2d339c8c1ad990a9d7':
        img.src = 'assets/imgs/CD7-jesus-hijo-de-dios-ii.jpg';
        break;
      case '5b3cec44339c8c1ad990a9d8':
        img.src = 'assets/imgs/CD8-Pasion-Resurreccion.jpg';
        break;
      default:
        img.src = 'assets/img/avatars/noavatar.png';
    }

    let imgCUstom =  img.src;
    document.getElementById('header').style.backgroundImage = "url('" + imgCUstom + "')";
    document.getElementById('header').style.backgroundSize = 'cover';
    document.getElementById('header').style.backgroundPosition = 'center';
    document.getElementById('header').style.boxShadow = "0 0 0 1000px rgba(0,0,0,0.61) inset";
  }

  async checkFile(path) {
    try {
      await this.platform.ready();
      if (this.platform.is('ios')) {
        this.storageDirectory = this.file.dataDirectory;
      }
      if (this.platform.is('android')) {
        this.storageDirectory = this.file.externalDataDirectory;
      }
      const resolvedDirectory = await this.file.resolveDirectoryUrl(this.storageDirectory);
      const data = await this.file.checkFile(resolvedDirectory.nativeURL, path);
      return data;
    } catch (err) {
      if (err.code === 1) {
        return null;
      }
      throw err;
    }
  }

  async play(song?) {
    this.presentLoadingDefault();
    // console.log(song);
    if (!this.dataService.getPseudoPlayer()) {
      await this.dataService.setPseudoPlayer(true);
      this.classHeader = 'header-song-activated';
      // this.play(song);
    }
    setTimeout(async ()=> {
      let listToPass: Array<any> = this.songs.slice();
      const index = listToPass.findIndex(element => element.name === song.name)
      await this.dataService.setSongsToPlay(listToPass);
      await this.dataService.setSongToPlay(song);
      await this.events.publish('playMusic');
      await this.dataService.setTimerReload(0);
    },500);
    this.dismissLoader();
    //this.navCtrl.push(MediaComponent, { song: song, songList: listToPass, componentSongList: this })
  }

  toggleFavorite() {
    const self = this;
    let user = this.dataService.getUserLoged();
    console.log(this.favorite);
    if (this.favorite) {
      user.favorites.splice(this.index, 1);
    }
    else {
      if (user.favorites) {
        user.favorites.push(this.album.id)
      }
      else {
        user.favorites = [];
        user.favorites.push(this.album.id)
      }
    }
    this.presentLoadingDefault();
    console.log(user);
    this.userService.update(user).then(result => {
      if (result.error) {
        console.log(result.error)
        self.dismissLoader();
      }
      else {
        console.log(result);
        self.dataService.setUserLoged(result.data);
        // self.favorite = !self.favorite;
        self.dismissLoader();
      }
    });
    self.favorite = !self.favorite;
    self.dismissLoader();
  }

  async prepareSongs(type?) {
    // console.log(type)
    // console.log(this.dataService.getSongs())
    let number = 1;
    if (type === 1) {
      this.dataService.getSongs().forEach(element => {

        const inCategory: boolean = element.categories.find(element => {
          return element._id === this.album.id;
        })

        if (inCategory) {
          element.pos = number;
          this.songs.push(element)
          number++
        }
      });
    } else if (type === 0) {

      this.dataService.getSongs().forEach(element => {

        const inAlbum: boolean = element.albums.find(element => {
          return element._id === this.album.id;
        })

        if (inAlbum) {
          element.pos = number;
          element.image = this.album.image;
          this.songs.push(element)
          number++
        }
      });
    }
  }

  set timer(value) {
    console.log(value)
    if (value === 0) {
      localStorage.removeItem('timer');
    }
    this.dataService.setTimer(+value)
    this.dataService.setTimerTemporizador(0);
    localStorage.setItem('timer',JSON.stringify(this.dataService.getTimer()));
  }

  get timer() {
    return this.dataService.getTimer();
  }
}
