import {PlayListPage} from './../playLists/playList.component';
import {BaseComponent} from './../../shared/components/base.component';
import {DonationPage} from './../donation/donation.component';
import {Component} from '@angular/core';
import {NavController, ToastController, LoadingController} from 'ionic-angular';
import {DataService} from "../../shared/services/dataService";
import {SongService} from "../../shared/services/songService";
import {UtilService} from "../../shared/services/utilService";
import {AlbumService} from "../../shared/services/albumService";
import {Album} from "../../shared/model/album";
import {Network} from "@ionic-native/network";
import {PaypalPage} from "../paypal/paypal.component";
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';

@Component({
  selector: 'page-home-donation',
  templateUrl: 'homeDonation.component.html'
})

export class HomeDonationPage extends BaseComponent {
  date: Date = new Date();
  fecha: any;
  username: String;
  pwd: String;
  items: Array<Album> = [];
  itemsCount: Array<any> = [];

  constructor(public navCtrl: NavController, toastCtrl: ToastController,
              protected loadingController: LoadingController,
              private dataService: DataService,
              private songService: SongService,
              private albumService: AlbumService,
              private utilService: UtilService,
              private network: Network,
              private nativePageTransitions: NativePageTransitions) {
    super(toastCtrl, loadingController)
    let options = {weekday: 'long', year: 'numeric', month: 'long', day: 'numeric'};
    this.fecha = this.date.toLocaleDateString('es-ES', options);
    if (this.dataService.getAutoLog()) {
      this.login();
    }

    this.getAvatar();
  }

  async getAvatar() {
    const responde = await this.dataService.getAvatarCustom();
    this.dataService.setAvatar(responde);
  }

  goToDonations() {
    // this.utilService.goToPage(DonationPage, 'root', 'slide');
    // this.navCtrl.setRoot(DonationPage);
    let options: NativeTransitionOptions = {
      direction: 'down',
      duration: 200,
      slowdownfactor: -1,
      iosdelay: 50
    };

    this.nativePageTransitions.slide(options);
    this.navCtrl.setRoot(DonationPage);
  }

  goToPaypal() {
    // this.utilService.goToPage(PaypalPage, 'root', 'slide');
    // this.navCtrl.setRoot(DonationPage);
    /*let options: NativeTransitionOptions = {
      direction: 'up',
      duration: 500,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 60
    };*/

    let options: NativeTransitionOptions = {
      direction: 'down',
      duration: 200,
      slowdownfactor: 3,
      iosdelay: 100,
      androiddelay: 150,
    };

    this.nativePageTransitions.slide(options);
    this.navCtrl.setRoot(PaypalPage);
  }

  async login() {
    let user = JSON.parse(localStorage.getItem('userRemembered'));
    // const userLog = this.dataService.getUserLoged();
    // this.dataService.setAvatar(localStorage.getItem('avatar'));
    this.dataService.setUserLoged(user);
    this.username = user.user;
    this.pwd = user.password;
    if (this.network.type === 'unknown' || this.network.type === 'none') {
      this.toastCtrl.create({
        message: 'No tiene conexión a internet',
        duration: 3000,
        position: 'top'
      }).present();

    } else {
      this.getAlbums();
    }
    //
    // this.presentLoadingDefault();
    // this.userService.login({ 'username': this.username.toLowerCase(), 'pwd': this.pwd }).then(async result => {
    //     console.log(result)
    //     if (result.error) {
    //         this.presentToastCenter('por favor verifica tus datos')
    //         this.navCtrl.setRoot(LoginPage)
    //         this.dismissLoader();
    //     }
    //     else {
    //         this.dismissLoader()
    //         this.dataService.setToken(result.data.token);
    //         this.dataService.setUserLoged(result.data.user);
    //         this.dataService.setAvatar(result.data.user.image && result.data.user.image || 'assets/img/avatars/noavatar.png');
    //         console.log(result)
    //         this.dataService.setRemember(true);
    //         await this.getSongs();
    //     }
    // })
  }
  getAlbums() {
    this.albumService.getAlbums().then(result => {
      if (result.error) {
        throw new Error(result.error);
      }
      else {
        if (result.data.albums) {
          result.data.albums.forEach(element => {
            let album = new Album(element);
            this.items.push(album);

            this.songService.getAllSongsAlbumCount(album.id).then(result => {
              if (result.error) {
                throw new Error(result.error);
              }else {
                console.log(result.data);
                if (result.data) {
                  this.itemsCount.push(result.data);
                  localStorage.setItem('songsCount', JSON.stringify(this.itemsCount));
                }
              }
            })
          });
          localStorage.setItem('albums', JSON.stringify(this.items));
        }
      }
    })
  }

  async getSongs() {
    this.presentLoadingDefault();
    await this.songService.getAllSongs().then(result => {
      if (result.error) {
        this.dataService.setSongs(JSON.parse(localStorage.getItem('songs')));
        let songs: Array<any> = JSON.parse(localStorage.getItem('songs'));
        if (songs.length === 0) {
          this.presentToastCenter(result.error)
        }
        this.dismissLoader();
      }
      else {
        console.log(result)
        this.dataService.setSwipe(true);
        this.dataService.setSongs(result.data);
        localStorage.setItem('songs', JSON.stringify(result.data))
        this.dismissLoader();
      }
    }).catch(error => {
      this.dismissLoader();
    })
  }

  goToPlayList() {
    // this.navCtrl.setRoot(PlayListPage);
    this.utilService.goToPage(PlayListPage, 'root');
  }
}
