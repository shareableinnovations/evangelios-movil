import { Component } from '@angular/core';
// import { DomSanitizer } from '@angular/platform-browser';
import { NavController, ToastController} from "ionic-angular";
import { Network } from "@ionic-native/network";
import { HomeDonationPage } from "../homeDonation/homeDonation.component";
import {UtilService} from "../../shared/services/utilService";
import {InAppBrowser} from "@ionic-native/in-app-browser";

@Component({
    selector: 'page-paypal',
    templateUrl: 'paypal.component.html'
})
export class PaypalPage {
    myurl: any = "";
    clicked: boolean = false;
    constructor(public navCtrl: NavController,
                private network: Network,
                private toastCtrl: ToastController,
                // private sanitizer: DomSanitizer,
                private utilService: UtilService,
                private iap: InAppBrowser) {
    }
    ngOnInit() {
        if (this.network.type != "none") {
            // this.myurl = this.sanitizer.bypassSecurityTrustResourceUrl('https://www.proterrasancta.org/es/ayudar-la-tierra-santa/tu-donacion/');
        } else {
            let toast = this.toastCtrl.create({
                message: "Por favor verifica tu conexión a internet",
                duration: 2000,
                position: 'middle'
            });
            toast.present();
        }
    }

  goToHome() {
    if (this.navCtrl.canGoBack()) {
      this.utilService.popPage('up');
    }
    else {
      this.utilService.goToPage(HomeDonationPage, 'root', 'up');
    }
  }

  goToDonate() {
      // this.clicked = true;
    this.iap.create("https://www.proterrasancta.org/","_system");
  }
}
