import { Component } from '@angular/core';
// import { DomSanitizer } from '@angular/platform-browser';
import { NavController, ToastController} from "ionic-angular";
import { Network } from "@ionic-native/network";
import { HomeDonationPage } from "../homeDonation/homeDonation.component";
import {UtilService} from "../../shared/services/utilService";
import { InAppBrowser } from '@ionic-native/in-app-browser';

@Component({
    selector: 'page-donation',
    templateUrl: 'donation.component.html'
})
export class DonationPage {
    myurl: any = "";
    clicked: boolean = false;
    constructor(public navCtrl: NavController,
                private network: Network,
                private toastCtrl: ToastController,
                // private sanitizer: DomSanitizer,
                // public navParams: NavParams,
                private utilService: UtilService,
                private iap: InAppBrowser) {
    }
    ngOnInit() {
        if (this.network.type !== 'none') {
            // this.myurl = this.sanitizer.bypassSecurityTrustResourceUrl('https://saxum.org/friends-of-saxum/donate/');

        } else {
            let toast = this.toastCtrl.create({
                message: "Por favor verifica tu conexión a internet",
                duration: 2000,
                position: 'middle'
            });
            toast.present();
        }
    }

  goToHome() {
    if (this.navCtrl.canGoBack()) {
      this.utilService.popPage('up');
    }
    else {
      this.utilService.goToPage(HomeDonationPage, 'root', 'up');
    }
  }

  goToDonate() {
      // this.clicked = true;
    this.iap.create("https://saxum.org/friends-of-saxum/donate/","_system");
  }
}
