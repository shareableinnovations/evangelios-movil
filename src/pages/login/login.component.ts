import { ForgotPage } from './../forgotPassword/forgotPassword.component';
import { HomeDonationPage } from './../homeDonation/homeDonation.component';
import { Component } from '@angular/core';
import { NavController, ToastController, LoadingController } from 'ionic-angular';
import { BaseComponent } from "../../shared/components/base.component";
import { MenuController } from 'ionic-angular/components/app/menu-controller';
import { UserService } from "../../shared/services/userService";
import { RegisterPage } from "../register/register.component";
import { DataService } from "../../shared/services/dataService";
import { SongService } from "../../shared/services/songService";
import { AlbumService } from "../../shared/services/albumService";
import { Album } from "../../shared/model/album";
import {InfoPage} from "../info/info";

@Component({
  selector: 'page-login',
  templateUrl: 'login.component.html'
})
export class LoginPage extends BaseComponent {
  items: Array<Album> = [];
  itemsCount: Array<any> = [];
  full: boolean;
  username: string = '';
  pwd: string = '';
  remember: boolean;
  constructor(protected toastCtrl: ToastController,
    public menuController: MenuController,
    public navController: NavController,
    private userService: UserService,
    private dataService: DataService,
    protected loadingController: LoadingController,
    private albumService: AlbumService,
    private songService: SongService) {

    super(toastCtrl, loadingController)
    this.full = false;
    this.dataService.setSwipe(false);
    if (this.dataService.getRemember()) {
      if (localStorage.getItem('userRemembered') !== "") {
        let user = JSON.parse(localStorage.getItem('userRemembered'));
        this.remember = this.dataService.getRemember();
        this.username = user.user;
        this.pwd = user.password;
        this.login();
      }
    }
    else {
      this.remember = false;
    }
  }

  login() {
    this.presentLoadingDefault();
    this.userService.login({ 'username': this.username.toLowerCase(), 'pwd': this.pwd }).then(async (result) => {
      if (result.error) {
        throw new Error('por favor verifica tus datos e intenta nuevamente')
      }
      else {
        this.dataService.setToken(result.data.token);
        this.dataService.setUserLoged(result.data.user);
        await this.dataService.setAvatarCustom(result.data.user.image && result.data.user.image || '../assets/img/avatars/noavatar.png');
        this.dataService.setRemember(true);
        localStorage.setItem('userRemembered', JSON.stringify(result.data.user))
        this.navController.setRoot(HomeDonationPage, {}, { animate: false });
        this.dismissLoader()
        /*if (this.remember) {
          localStorage.setItem('userRemembered', JSON.stringify(result.data.user))
        }
        else {
          localStorage.setItem('userRemembered', '');
        }*/
      }
    })
      .then(async () => await this.getSongs())
      .then(() => this.getAlbums())
      .catch(error => {
        console.log(error.toString())

        let err:string = error.toString();

        // console.log(err.toLowerCase)
        if (err === 'Error: por favor verifica tus datos e intenta nuevamente') {
          this.presentToastCenter("Credenciales Invalidas")
        } else {
          if (err === "TypeError: Cannot read property 'token' of undefined") {
            this.presentToastCenter("error: verifique su conexión a internet")
          }
          else {
            this.presentToastCenter(error)
          }
        }

        this.dismissLoader()
      })
  }

  explorar() {
    this.presentLoadingDefault();
    this.userService.login({ 'username': 'demo@jesusdenazart.cl', 'pwd': 123456 }).then(async (result) => {
      if (result.error) {
        throw new Error('por favor verifica tus datos e intenta nuevamente')
      }
      else {
        this.dataService.setToken(result.data.token);
        this.dataService.setUserLoged(result.data.user);
        await this.dataService.setAvatarCustom(result.data.user.image && result.data.user.image || '../assets/img/avatars/noavatar.png');
        this.dataService.setRemember(true);
        localStorage.setItem('userRemembered', JSON.stringify(result.data.user))
        this.navController.setRoot(HomeDonationPage, {}, { animate: false });
        this.dismissLoader()
        /*if (this.remember) {
          localStorage.setItem('userRemembered', JSON.stringify(result.data.user))
        }
        else {
          localStorage.setItem('userRemembered', '');
        }*/
      }
    })
      .then(async () => await this.getSongs())
      .then(() => this.getAlbums())
      .catch(error => {
        console.log(error.toString())

        let err:string = error.toString();

        // console.log(err.toLowerCase)
        if (err === 'Error: por favor verifica tus datos e intenta nuevamente') {
          this.presentToastCenter("Credenciales Invalidas")
        } else {
          if (err === "TypeError: Cannot read property 'token' of undefined") {
            this.presentToastCenter("error: verifique su conexión a internet")
          }
          else {
            this.presentToastCenter(error)
          }
        }

        this.dismissLoader()
      })
  }



  goToRegisterPage() {
    this.navController.push(RegisterPage, {}, { animate: false });
  }

  async getSongs() {
    await this.songService.getAllSongs().then(result => {
      if (result.error) {
        throw new Error(result.error);
      }
      else {
        // this.dataService.setSwipe(true);
        this.dataService.setSongsCustom(result.data);
        // this.navController.setRoot(HomeDonationPage, {}, { animate: false });
      }
    })
  }

  getAlbums() {
    this.albumService.getAlbums().then(result => {
      if (result.error) {
        throw new Error(result.error);
      }
      else {
        result.data.albums.forEach(element => {
          let album = new Album(element);
          this.items.push(album);

          this.songService.getAllSongsAlbumCount(album.id).then(result => {
            if (result.error) {
              throw new Error(result.error);
            }else {
              console.log(result.data);
              this.itemsCount.push(result.data);
              localStorage.setItem('songsCount', JSON.stringify(this.itemsCount));
            }
          })
        });
        localStorage.setItem('albums', JSON.stringify(this.items));
        this.navController.setRoot(HomeDonationPage, {}, { animate: false });
        this.dismissLoader()
      }
    })
  }

  checkParameters() {
    this.full = false;
    if (this.username != '' && this.pwd != '' && this.validateEmail()) {
      this.full = true;
    }
  }

  validateEmail() {
    var re = /\S+@\S+\.\S+/;
    return re.test(this.username);
  }


  goToForgotPassword() {
    this.navController.push(ForgotPage, {}, { animate: false })
  }
}
