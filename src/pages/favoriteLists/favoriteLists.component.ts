import { BaseComponent } from "../../shared/components/base.component";
import { Component } from "@angular/core";
import { ToastController, LoadingController, NavController } from "ionic-angular";
import { DataService } from "../../shared/services/dataService";
import { AlbumService } from "../../shared/services/albumService";
import { Album } from "../../shared/model/album";
import { SongsPage } from "../songList/songList.component";
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import {SongService} from "../../shared/services/songService";

@Component({
    selector: 'page-favoriteList',
    templateUrl: 'favoriteLists.component.html'
})
export class FavoriteListPage extends BaseComponent {
    items = [];
    itemsCount = [];
  constructor(protected toastCtrl: ToastController, protected loadingController: LoadingController,
        private dataService: DataService, private albumService: AlbumService, private navCtrl: NavController,
        private nativePageTransitions: NativePageTransitions,
              private songService: SongService
    ) {
        super(toastCtrl, loadingController)
    }


    async ngOnInit() {
        this.getAllAlbums();
    }

    getAllAlbums() {
        if (this.dataService.getUserLoged().favorites) {
            this.presentLoadingDefault();
            let user = (this.dataService.getUserLoged())
            this.albumService.getAlbums().then(result => {
                if (result.error) {
                  //console.log(result.error, 'result.error');
                    this.items = JSON.parse(localStorage.getItem('albums'));
                    if (this.items && this.items.length > 0) {

                    }
                    else {
                        this.presentToastCenter(result.error)
                    }
                    this.dismissLoader()
                }
                else {
                  //console.log(result.data, 'result.data');
                  if (result.data && result.data.albums) {
                    result.data.albums.forEach(element => {
                      let album = new Album(element);
                      let index = user.favorites.findIndex(element => element === album.id);
                      if (index !== -1) {
                        this.items.push(element);
                      }
                    });
                  }
                    this.dismissLoader()
                }
            })
        }
    }

    showAlbum(album) {
      let options: NativeTransitionOptions = {
        direction: 'up',
        duration: 500,
        slowdownfactor: 3,
        slidePixels: 20,
        iosdelay: 100,
        androiddelay: 150,
        fixedPixelsTop: 0,
        fixedPixelsBottom: 60
      };
      this.nativePageTransitions.fade(options)
        .then(onSuccess=>{})
        .catch(onError=>{});
        this.navCtrl.push(SongsPage, { album: album, type: 0 },{animate: false});
    }
}
