import { UserService } from './../../shared/services/userService';
import { User } from './../../shared/model/user';
import { DataService } from './../../shared/services/dataService';
import { BaseComponent } from "../../shared/components/base.component";
import { Component } from "@angular/core";
import { NavController } from "ionic-angular/navigation/nav-controller";
import { HomePage } from "../home/home";
import { ToastController, LoadingController, Platform } from "ionic-angular";
import { ImagePicker } from "@ionic-native/image-picker";
import { AndroidPermissions } from "@ionic-native/android-permissions";
import { Camera, CameraOptions } from "@ionic-native/camera";

@Component({
    selector: 'page-config',
    templateUrl: 'config.component.html'
})
export class ConfigPage extends BaseComponent {
    user: User;
    options: any;
    avatar: any;
    constructor(protected toastCtrl: ToastController, private navCtrl: NavController, protected loadingController: LoadingController,
        private dataService: DataService, public platform: Platform, private imagePicker: ImagePicker,
        private androidPermissions: AndroidPermissions, private userService: UserService,
        private camera: Camera) {
        super(toastCtrl, loadingController);
        const self = this;
        this.user = this.dataService.getUserLoged();
        this.platform.ready().then(() => {
            if (this.platform.is('ios')) {

            } else if (this.platform.is('android')) {
                console.log('entre');
                self.options = { maximumImagesCount: 1 };
                self.checkPermissions()
            }
        });

    }

    logout() {
        localStorage.setItem('userRemembered', '')
        this.navCtrl.setRoot(HomePage)
    }

    pickImage() {
        this.takePhoto(0);
    }


    takePhoto(sourceType: number) {
        const self = this;
        const options: CameraOptions = {
            quality: 50,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            correctOrientation: true,
            sourceType: sourceType,
        }

        this.camera.getPicture(options).then((imageData) => {
            let base64Image = 'data:image/jpeg;base64,' + imageData;
            this.user.image = base64Image;
            self.presentLoadingDefault()
            self.userService.updateUser(this.user).then(result => {
                if (result.error) {
                    self.dismissLoader()
                } else {
                    this.avatarSet(base64Image);
                    self.dismissLoader()
                }
            })
        }, (err) => {
            // Handle error
        });
    }

    async avatarSet(base64Image) {
      const self = this;
      await self.dataService.setAvatar(base64Image);
    }

    checkPermissions() {
        this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE)
            .then(result => {
                if (result.hasPermission === false) {
                    this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE])
                }
            })
    }

}
