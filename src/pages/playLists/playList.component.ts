import { BaseComponent } from "../../shared/components/base.component";
import { Component } from "@angular/core";
import { NavController } from "ionic-angular/navigation/nav-controller";
import { SongsPage } from "../songList/songList.component";
import { ToastController, LoadingController } from "ionic-angular";
import { Album } from "../../shared/model/album";
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import {HomeDonationPage} from "../homeDonation/homeDonation.component";
import {AlbumService} from "../../shared/services/albumService";
import {SongService} from "../../shared/services/songService";
import {DataService} from "../../shared/services/dataService";

@Component({
    selector: 'page-playList',
    templateUrl: 'playList.component.html'
})
export class PlayListPage extends BaseComponent {
    items: Array<Album> = [];
    base64Image: any;
    itemsCount: Array<any> = [];
    constructor(protected toastCtrl: ToastController, private navCtrl: NavController,
        private nativePageTransitions: NativePageTransitions,
        protected loadingController: LoadingController,
                private albumService: AlbumService,
                private songService: SongService,
                private dataService:  DataService) {
        super(toastCtrl, loadingController)
    }

    async ngOnInit() {
      this.getAlbums();
    }

    getAlbums() {
        this.items = JSON.parse(localStorage.getItem('albums'));
        if (!(this.items && this.items.length > 0)) {
          this.presentToastCenter('Ha habido un problema para recuperar los albums')
        } else {
          this.getAlbumsLoad();
        }
    }

    getAlbumsLoad() {
      this.albumService.getAlbums().then(result => {
        if (result.error) {
          throw new Error(result.error);
        }
        else {
          if (result.data.albums) {
            result.data.albums.forEach(element => {
              let album = new Album(element);
              let index = this.items.findIndex(element => element.id === album.id);
              if (index === -1) {
                this.items.push(element);
              }
              this.songService.getAllSongsAlbumCount(album.id).then(result => {
                if (result.error) {
                  throw new Error(result.error);
                }else {
                  console.log(result.data);
                  if (result.data) {
                    this.itemsCount.push(result.data);
                    this.dataService.setSongsCount(this.itemsCount);
                    localStorage.setItem('songsCount', JSON.stringify(this.itemsCount));
                  }
                }
              })
            });
            localStorage.setItem('albums', JSON.stringify(this.items));
          }
        }
      })
    }

    showAlbum(album) {
      // this.presentLoadingDefault();
      let options: NativeTransitionOptions = {
        direction: 'up',
        duration: 500,
        slowdownfactor: 3,
        slidePixels: 20,
        iosdelay: 100,
        androiddelay: 150,
        fixedPixelsTop: 0,
        fixedPixelsBottom: 60
      };
      this.nativePageTransitions.fade(options)
        .then(()=>{})
        .catch(()=>{});
        this.navCtrl.push(SongsPage,{ album: album, type: 0 },{animate: false});
        // this.dismissLoader();
    }
}
