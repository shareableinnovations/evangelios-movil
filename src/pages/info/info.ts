import {Component} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';
import {NavController, NavParams, ToastController} from "ionic-angular";
import {Network} from "@ionic-native/network";
import {UtilService} from "../../shared/services/utilService";
import {PlayListPage} from "../playLists/playList.component";

@Component({
  selector: 'page-info',
  templateUrl: 'info.html'
})
export class InfoPage {
  myurl: any = "";
  clicked: boolean = false;
  dataReceive: any;
  album: any;

  constructor(public navCtrl: NavController,
              private network: Network,
              private toastCtrl: ToastController,
              private sanitizer: DomSanitizer,
              public navParams: NavParams,
              private utilService: UtilService) {
    this.dataReceive = navParams.data;
  }

  ngOnInit() {
    this.album = this.dataReceive.params;
    console.log(this.album);
  }

  goToHome() {
    if (this.navCtrl.canGoBack()) {
      this.utilService.popPage('up');
    }
    else {
      this.utilService.goToPage(PlayListPage, 'root' );
    }
  }
}
